#!/usr/bin/env python
# coding: utf-8

# %% Import packages
import numpy as np
import os
#import pandas as pd
import xarray as xr
from PIL import ImageDraw, Image
from shapely.ops import transform
import shapely.geometry as sg # Operations on shapes (points, lines and polygons)
from pyproj import CRS # Coordinate Reference System (map projections)
import geopandas as gpd # Geospatial extension of Pandas

# %% Paths
DATA_PATH = os.path.join('DATA', 'SOURCE')
GEO_PATH = os.path.join(DATA_PATH, 'Geographical', 'NaturalEarth')
GEO_PATH_OUT = os.path.join('DATA', 'PREPROCESSED', 'Geographical')
os.makedirs(GEO_PATH_OUT, exist_ok=True)

name_dict={
 'countries':'ne_10m_admin_0_countries',
 'provinces':'ne_10m_admin_1_states_provinces'
 }

# %% Ingest Natural Earth Data 
geographic_features = dict()
for key in name_dict.keys():
    shapefile_path = os.path.join(GEO_PATH, name_dict[key], name_dict[key]+'.shp')
    geographic_features[key] = gpd.read_file(shapefile_path)

# %% Pull countries
countries = geographic_features['countries'] # Get the GeoDataFrame for 'countries'
canada_table = countries.loc[countries['ADMIN']=='Canada'] # Select the country named 'Canada'
canada = canada_table['geometry'].values[0] # Pull the geometry of Canada
usa = countries.loc[countries['ADMIN']=='United States of America']['geometry'].values[0]
conus = usa[0]
greenland = countries.loc[countries['ADMIN']=='Greenland']['geometry'].values[0]

# %% Pull provinces
provinces = geographic_features['provinces']
alaska = provinces.loc[provinces['name']=='Alaska']['geometry'].values[0]
provinces_gdf = provinces.loc[provinces['iso_a2']=='CA']

# %% Map projection
ll_crs = CRS.from_proj4("+proj=longlat +ellps=WGS84 +datum=WGS84 +no_defs")

# %% Read model once
era5_path = os.path.join(DATA_PATH, 'ERA5-Land', 'tp', 'hourly')
era5_file = os.path.join(era5_path, "ERA5-Land_tp_NA40N_hourly_2012-01.nc")
DS = xr.open_dataset(era5_file)
time_select_grid = (DS.coords['time'].dt.hour==0)&(DS.coords['time'].dt.day==1)
precip_select_grid = DS.tp.isel(time=time_select_grid)*1000
ocean_mask = np.isnan(precip_select_grid)

# %% Grid in polygon
def get_bbox(xdata):
    lat_min = xdata.coords['latitude'].min().values
    lat_max = xdata.coords['latitude'].max().values
    lon_min = xdata.coords['longitude'].min().values
    lon_max = xdata.coords['longitude'].max().values
    return [lon_min, lon_max, lat_min, lat_max]

def get_dim(xdata):
    ny = len(DS.coords['latitude'])
    nx = len(DS.coords['longitude'])
    return (nx, ny)

def transform_coords(shape, bbox, dim):
    [nx, ny] = dim
    [lon_min, lon_max, lat_min, lat_max] = bbox
    def normalize(x, y):
        coords_x = (x-lon_min)/(lon_max-lon_min)*(nx-1)
        coords_y = (y-lat_min)/(lat_max-lat_min)*(ny-1)
        return (coords_x, coords_y)
    return transform(normalize, shape)

def fill_polygon(image, polygon):
    draw = ImageDraw.Draw(image)
    coords = polygon.exterior.coords[:]
    draw.polygon(xy=coords, fill=1)
    for hole in polygon.interiors:
        coords = hole.coords[:]
        draw.polygon(xy=coords, fill=0)
    return np.array(image)

def polygon2grid(xdata, polygon):
    """
    polygon: Shapely.Geometry
    grid: DataArray
    """
    # Get grid dimension
    dim = get_dim(xdata)
    # Get grid bounds
    bbox = get_bbox(xdata)
    # Create PIL binary (black and white) image, filled with zeros
    image = Image.new('1', dim)
    # Extract coordinate from Shapely Polygon
    polygon_ij = transform_coords(polygon, bbox, dim)
    # Find coordinates inside polygon, filled with ones
    mask = np.zeros((dim[1],dim[0]))
    if polygon_ij.geom_type == 'MultiPolygon':
        for part in polygon_ij:
            new_mask = fill_polygon(image, part)
            assert np.all(new_mask == np.array(image))
            mask = np.logical_or(mask, new_mask)
    elif polygon_ij.geom_type == 'Polygon':
        new_mask = fill_polygon(image, polygon_ij)
        assert np.all(new_mask == np.array(image))
        mask = np.logical_or(mask, new_mask)
    else:
        print("Error: {polygon_ij.geom_type} is an unexpected geom_type")
    # Resulting image is a mask with ones inside polygon
    return mask


# Create geographic masks
canada_mask = polygon2grid(precip_select_grid, canada)
alaska_mask = polygon2grid(precip_select_grid, alaska)
conus_mask = polygon2grid(precip_select_grid, conus)
greenland_mask = polygon2grid(precip_select_grid, greenland)

# Create geographic mask for Canadian provinces
dim = get_dim(precip_select_grid)
mask = np.zeros((dim[1], dim[0]))
dict_of_provinces = {}
for (k, (row, province)) in enumerate(provinces_gdf.iterrows()):
    dict_of_provinces[k+1] = (province['name'], province['geometry'])
    province_mask = polygon2grid(precip_select_grid, province['geometry'])
    mask[province_mask] = k+1
    #plt.figure()
    #plt.imshow(province_mask, origin='lower', cmap='cool')
    #plt.title(province['name'])
    #plt.show()
mask[conus_mask] = k+2
dict_of_provinces[k+2] = ('conus', conus)
mask[alaska_mask] = k+3
dict_of_provinces[k+3] = ('alaska', alaska)
mask[greenland_mask] = k+4
dict_of_provinces[k+4] = ('greenland', greenland)
mask = np.flipud(mask)
mask[ocean_mask.values.squeeze()] = np.nan


# %% Fill gaps near land-water boundary
[yis, xis] = np.where(mask==0)
lat_i = DS.latitude[yis]
lon_i = DS.longitude[xis]
coords = np.array([lon_i, lat_i]).T
grid_pts = sg.MultiPoint(coords)
for (point, xi, yi) in zip(grid_pts, xis, yis):
    min_distance = np.inf
    for (index, item) in dict_of_provinces.items():
        (name, province) = item
        distance = province.distance(point)
        if distance < min_distance:
            closest_province_index = index
            min_distance = distance
    mask[yi,xi] = closest_province_index

# %% Export province mask
mask_xr = xr.DataArray(data=mask-1, 
             coords={'longitude':DS.coords['longitude'], 
                     'latitude':DS.coords['latitude']},
             dims=['latitude','longitude'],
             name='geographic_mask')
mask_xr.to_netcdf(path=os.path.join(GEO_PATH_OUT, 'geographic_mask.nc'))

# %% Export geodataframe
canada_gdf = provinces_gdf[['admin','name','geometry']]
canada_gdf = canada_gdf.rename(columns={'name':'Name', 'admin': 'Country'})

greenland_gdf = countries.loc[countries['ADMIN']=='Greenland'][['ADMIN','NAME','geometry']]
greenland_gdf = greenland_gdf.rename(columns={'NAME':'Name', 'ADMIN': 'Country'})

usa_gdf = gpd.GeoDataFrame(data={'admin':['United States of America', 'United States of America'], 
                       'name':['CONUS', 'Alaska'], 
                       'geometry': [conus, alaska]})
usa_gdf = usa_gdf.rename(columns={'name':'Name', 'admin': 'Country'})

NA_gdf = canada_gdf.append(usa_gdf).append(greenland_gdf)
NA_gdf = NA_gdf.reset_index().drop(columns=['index'])
NA_gdf.crs = ll_crs

NA_gdf.to_file(os.path.join(GEO_PATH_OUT ,'Provinces-States.shp'))

