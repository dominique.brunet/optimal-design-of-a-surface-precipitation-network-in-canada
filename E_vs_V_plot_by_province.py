# -*- coding: utf-8 -*-
"""
Created on Fri Dec 10 11:57:56 2021

@author: BrunetD
"""

import xarray as xr
from matplotlib import pyplot as plt
import matplotlib.colors as mcolors
import numpy as np
from glob import glob
import seaborn as sns
import os
import pandas as pd

# %%
FIGS_PATH = ".."
DATA_PATH = ".."

# %% 
list_of_files = glob(os.path.join(DATA_PATH,'prior-stats-*.nc'))
list_of_files

# %% Categorical colors
#provinces = [os.path.basename(file_).split('.')[0].split('-')[2].replace('_',' ') for file_ in list_of_files]
colors = list(mcolors.TABLEAU_COLORS.keys())+['navy','lime','gold']
#color_dict = {province:color for (province, color) in zip(provinces, colors)}
    
# %% Organize data into dataframe
E_list, V_list, P_list = [], [], []
for file_ in list_of_files:
    province = os.path.basename(file_).split('.')[0].split('-')[2].replace('_',' ')
    DS_prior = xr.open_dataset(file_)
    E_X = DS_prior.E_TP
    V_X = DS_prior.V_TP
    n = int(np.ceil(len(E_X)/1000))
    E_list = E_list + list(E_X.values[::n])
    V_list = V_list + list(np.sqrt(V_X.values[::n]))
    P_list = P_list + [province]*len(E_X.values[::n])
df = pd.DataFrame(data={'Expected Precipitation (mm/day)':E_list,
                        'Precipitation Standard Deviation (mm/day)':V_list,
                        'Province/Territory':P_list})

# %% Matplotlib fontsize configuration
SMALL_SIZE = 14
MEDIUM_SIZE = 16
BIGGER_SIZE = 18

plt.rc('font', size=SMALL_SIZE)          # controls default text sizes
plt.rc('axes', titlesize=SMALL_SIZE)     # fontsize of the axes title
plt.rc('axes', labelsize=MEDIUM_SIZE)    # fontsize of the x and y labels
plt.rc('xtick', labelsize=SMALL_SIZE)    # fontsize of the tick labels
plt.rc('ytick', labelsize=SMALL_SIZE)    # fontsize of the tick labels
plt.rc('legend', fontsize=SMALL_SIZE)    # legend fontsize
plt.rc('figure', titlesize=BIGGER_SIZE)  # fontsize of the figure title

# %% Plotting
fig = plt.figure()
g = sns.jointplot(data=df, 
             height=12,
              x='Expected Precipitation (mm/day)', 
              y='Precipitation Standard Deviation (mm/day)', 
              hue='Province/Territory', 
              palette=colors,
              #ax=ax, 
              alpha=0.1)
ax = g.ax_joint
ax.set_xscale('log')
ax.set_yscale('log')

# %%
fig.savefig(os.path.join(FIGS_PATH,'Figure_E_vs_V.png'), bbox_inches='tight')
