# -*- coding: utf-8 -*-
"""
Dominique Brunet
November 3, 2022
"""

# Built-in packages
from glob import glob
import os

# Installed packages
import numpy as np
import xarray as xr

# %%
DATA_PATH = os.path.join('DATA', 'SOURCE')

# %% Prepare daily temperature data
t2m_folder_hourly = os.path.join(DATA_PATH,'ERA5-Land','t2m','hourly')
t2m_path_list = glob(os.path.join(t2m_folder_hourly,'ERA5-Land_t2m_NA40N_hourly_*.nc'))
t2m_folder_daily = t2m_folder_hourly.replace('hourly','daily').replace('SOURCE','PREPROCESSED')
os.makedirs(t2m_folder_daily, exist_ok=True)

for t2m_path in t2m_path_list:
    t2m = xr.open_dataset(t2m_path)
    #t2m = t2m.t2m[:,ymin:ymax,xmin:xmax]
    
    t2m = t2m - 273.15 # K to degC
    t2m_daily = t2m.resample(time='1D').mean(dim='time')

    dest_path = t2m_path.replace('hourly','daily')
    print(f"Saved {dest_path}")
    t2m_daily.to_netcdf(dest_path)


# %% Prepare daily wind speed data
u10_folder_hourly = os.path.join(DATA_PATH,'ERA5-Land','u10','hourly')
v10_folder_hourly = u10_folder_hourly.replace('u10','v10')
u10_path_list = glob(os.path.join(u10_folder_hourly,'ERA5-Land_u10_NA40N_hourly_*.nc'))
v10_path_list = glob(os.path.join(v10_folder_hourly,'ERA5-Land_v10_NA40N_hourly_*.nc'))
ws_folder_daily = u10_folder_hourly.replace('hourly','daily').replace('u10','ws').replace('SOURCE','PREPROCESSED')
os.makedirs(ws_folder_daily, exist_ok=True)

for (u10_path, v10_path) in zip(sorted(u10_path_list), sorted(v10_path_list)):
    u10 = xr.open_dataset(u10_path)
    v10 = xr.open_dataset(v10_path)
    ws = np.sqrt(u10.u10**2+v10.v10**2)
    ws_daily = ws.resample(time='1D').mean(dim='time')

    ws_daily = ws_daily.to_dataset(name='ws')
    ws_path_daily = u10_path.replace('hourly','daily').replace('u10','ws')
    print(f"Saved {ws_path_daily}")
    ws_daily.to_netcdf(ws_path_daily)

# %% Prepare precipitation data
tp_folder_hourly = os.path.join(DATA_PATH,'ERA5-Land','tp','hourly')
tp_folder_daily = tp_folder_hourly.replace('hourly','daily').replace('SOURCE','PREPROCESSED')
os.makedirs(tp_folder_daily, exist_ok=True)
tp_path_list = glob(os.path.join(tp_folder_hourly,'ERA5-Land_tp_NA40N_hourly_*.nc'))
tp_path_list.sort(reverse=True) # in-place # go from last month to first
tp_first = [] # len = 0

for tp_path in tp_path_list:
    tp = xr.open_dataset(tp_path)
    tp = tp*1000 # to mm
    tp_daily = tp.tp
    
    # 1) get daily data for all days of the month
    tp_daily = tp_daily[::24,:,:]
    # 2) concat with first day of next month, if available
    if len(tp_first)>0:
        tp_daily = xr.concat([tp_daily, tp_first], dim='time')
    # 3) get first day of the month
    tp_first = tp_daily[0,:,:]
    # 4) shift to previous day
    tp_daily = tp_daily.shift(time=-1)
    # 5) Remove last day which is now NaN
    tp_daily = tp_daily[:-1]
    
    tp_daily = tp_daily.to_dataset(name='tp')
    tp_path_daily = tp_path.replace('hourly','daily')
    print(f"Saved {tp_path_daily}")
    tp_daily.to_netcdf(tp_path_daily)

