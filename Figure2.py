# -*- coding: utf-8 -*-
"""
Created on Thu Jan 20 12:24:50 2022

@author: BrunetD
"""

import xarray as xr
from matplotlib import pyplot as plt
import numpy as np
#import seaborn as sns
import os
import geopandas as gpd

plt.rc('font', size=18)

# %%
DATA_PATH = "DATA"
FIGS_PATH = "FIG"
RESULTS_PATH = "RESULTS"
tp_file = 'ERA5-Land_tp_NA40N_daily_aggregated.nc'

# %%
full_xr = xr.open_dataset(os.path.join(RESULTS_PATH, tp_file))

# %% Add CRS to xarray
from pyproj import CRS
crs = CRS("EPSG:4326") # ERA5 is on a 6367.47km sphere
full_xr.rio.write_crs(crs.to_string(), inplace=True)

# %% pad
full_xr = full_xr.ffill(dim='longitude', limit=2)
full_xr = full_xr.bfill(dim='longitude', limit=2)
full_xr = full_xr.ffill(dim='latitude', limit=2)
full_xr = full_xr.bfill(dim='latitude', limit=2)

# %% Reproject!
import rasterio
ae_crs = CRS.from_proj4('+proj=aea +lat_1=50 +lat_2=70 +lat_0=40 +lon_0=-99.5 +x_0=0 +y_0=0 +datum=NAD83 +units=m +no_defs')
full_xr_ae = full_xr.rio.reproject(ae_crs, nodata=np.nan, resampling=rasterio.enums.Resampling(1)) # bilinear interpolation
full_xr_ae.V_A.values = np.sqrt(full_xr_ae.V_A.values)
full_xr_ae.V_P.values = np.sqrt(full_xr_ae.V_P.values)
full_xr_ae.E_error.values = np.sqrt(full_xr_ae.E_error.values)
#full_xr_ae.EVVE.values = np.sqrt(full_xr_ae.EVVE.values)

full_xr_ae.V_A_cool.values = np.sqrt(full_xr_ae.V_A_cool.values)
full_xr_ae.V_P_cool.values = np.sqrt(full_xr_ae.V_P_cool.values)
full_xr_ae.E_error_cool.values = np.sqrt(full_xr_ae.E_error_cool.values)

full_xr_ae.V_A_warm.values = np.sqrt(full_xr_ae.V_A_warm.values)
full_xr_ae.V_P_warm.values = np.sqrt(full_xr_ae.V_P_warm.values)
full_xr_ae.E_error_warm.values = np.sqrt(full_xr_ae.E_error_warm.values)


# %% Add attributes
full_xr_ae.E_error.attrs["long_name"] = "Measurement Uncertainty of Daily Precipitation"
full_xr_ae.E_error.attrs["units"] = "%" 
full_xr_ae.V_A.attrs["long_name"] = "Standard Deviation of Daily Precipitation Amount"
full_xr_ae.V_A.attrs["units"] = "mm" 
full_xr_ae.E_A.attrs["long_name"] = "Expected Daily Precipitation Amount"
full_xr_ae.E_A.attrs["units"] = "mm" 
full_xr_ae.V_P.attrs["long_name"] = "Standard Deviation of Probability of Precipitation"
full_xr_ae.V_P.attrs["units"] = "%" 
full_xr_ae.E_P.attrs["long_name"] = "Expected Probability of Precipitation"
full_xr_ae.E_P.attrs["units"] = "%" 
full_xr_ae.phase.attrs["long_name"] = "Precipitation Phase Index"
full_xr_ae.phase.attrs["units"] = ""
full_xr_ae.windcat.attrs["long_name"] = "Wind Speed Index"
full_xr_ae.windcat.attrs["units"] = "" 

full_xr_ae.E_error_cool.attrs["long_name"] = "Measurement Uncertainty of Daily Precipitation for Cool Season"
full_xr_ae.E_error_cool.attrs["units"] = "%" 
full_xr_ae.V_A_cool.attrs["long_name"] = "Standard Deviation of Daily Precipitation Amount for Cool Season"
full_xr_ae.V_A_cool.attrs["units"] = "mm" 
full_xr_ae.E_A_cool.attrs["long_name"] = "Expected Daily Precipitation Amount for Cool Season"
full_xr_ae.E_A_cool.attrs["units"] = "mm" 
full_xr_ae.V_P_cool.attrs["long_name"] = "Standard Deviation of Probability of Precipitation for Cool Season"
full_xr_ae.V_P_cool.attrs["units"] = "%" 
full_xr_ae.E_P_cool.attrs["long_name"] = "Expected Probability of Precipitation for Cool Season"
full_xr_ae.E_P_cool.attrs["units"] = "%" 
full_xr_ae.phase_cool.attrs["long_name"] = "Precipitation Phase Index for Cool Season"
full_xr_ae.phase_cool.attrs["units"] = ""
full_xr_ae.windcat_cool.attrs["long_name"] = "Wind Speed Index for Cool Season"
full_xr_ae.windcat_cool.attrs["units"] = "" 

full_xr_ae.E_error_warm.attrs["long_name"] = "Measurement Uncertainty of Daily Precipitation for Warm Season"
full_xr_ae.E_error_warm.attrs["units"] = "%" 
full_xr_ae.V_A_warm.attrs["long_name"] = "Standard Deviation of Daily Precipitation Amount for Warm Season"
full_xr_ae.V_A_warm.attrs["units"] = "mm" 
full_xr_ae.E_A_warm.attrs["long_name"] = "Expected Daily Precipitation Amount for Warm Season"
full_xr_ae.E_A_warm.attrs["units"] = "mm" 
full_xr_ae.V_P_warm.attrs["long_name"] = "Standard Deviation of Probability of Precipitation for Warm Season"
full_xr_ae.V_P_warm.attrs["units"] = "%" 
full_xr_ae.E_P_warm.attrs["long_name"] = "Expected Probability of Precipitation for Warm Season"
full_xr_ae.E_P_warm.attrs["units"] = "%" 
full_xr_ae.phase_warm.attrs["long_name"] = "Precipitation Phase Index for Warm Season"
full_xr_ae.phase_warm.attrs["units"] = ""
full_xr_ae.windcat_warm.attrs["long_name"] = "Wind Speed Index for Warm Season"
full_xr_ae.windcat_warm.attrs["units"] = "" 

full_xr_ae.E_error.attrs["long_name"] = "Measurement Uncertainty of Daily Precipitation"
full_xr_ae.E_error.attrs["units"] = "%" 
full_xr_ae.V_A.attrs["long_name"] = "Standard Deviation of Daily Precipitation Amount"
full_xr_ae.V_A.attrs["units"] = "mm" 
full_xr_ae.E_A.attrs["long_name"] = "Expected Daily Precipitation Amount"
full_xr_ae.E_A.attrs["units"] = "mm" 
full_xr_ae.V_P.attrs["long_name"] = "Standard Deviation of Probability of Precipitation"
full_xr_ae.V_P.attrs["units"] = "%" 
full_xr_ae.E_P.attrs["long_name"] = "Expected Probability of Precipitation"
full_xr_ae.E_P.attrs["units"] = "%" 
full_xr_ae.phase.attrs["long_name"] = "Precipitation Phase Index"
full_xr_ae.phase.attrs["units"] = ""
full_xr_ae.windcat.attrs["long_name"] = "Wind Speed Index"
full_xr_ae.windcat.attrs["units"] = "" 

#full_xr_ae.EVVE.attrs["long_name"] = "Total Uncertainty of Daily Precipitation"
#full_xr_ae.EVVE.attrs["units"] = "mm"

full_xr_ae.x.attrs["long_name"] = "Longitude"
full_xr_ae.x.attrs["units"] = "degrees"
full_xr_ae.y.attrs["long_name"] = "Latitude"
full_xr_ae.y.attrs["units"] = "degrees"

# %% Clip
full_xr_ae = full_xr_ae.rio.clip_box(
    minx=-4500000,
    maxy=4950000,
    maxx=4500000,
    miny=350000)

# %% Add geographic layers
#PATH = '/home/dbr000/sitestore3/DATA'
GEO_PATH = os.path.join(DATA_PATH, 'Geographical', 'NaturalEarth')
shapefile_path = os.path.join(GEO_PATH, "Cultural", "Countries", 
                         "ne_10m_admin_0_countries.shp")
countries = gpd.read_file(shapefile_path)
shapefile_path = os.path.join(GEO_PATH, 'Cultural', 'States_Provinces', 
                             "ne_10m_admin_1_states_provinces.shp")
provinces = gpd.read_file(shapefile_path)
shapefile_path = os.path.join(GEO_PATH, 'Cultural', 'Graticules', 
                             "ne_50m_graticules_30.shp")
graticules = gpd.read_file(shapefile_path)

# %% Reproject geographic layers
countries_ae = countries.to_crs(ae_crs)
provinces_ae = provinces.to_crs(ae_crs)
graticules_ae = graticules.to_crs(ae_crs)

# %% Plot
def make_plot1(DA):
    fig = plt.figure(figsize=(10,4))
    axin = plt.gca()
    plt.axis([-4500000,4500000,350000,4950000])
    # geographic layer
    countries_ae.loc[countries_ae['ADMIN']!='Canada'].plot(ax=axin, color='lightgrey')
    countries_ae.boundary.plot(ax=axin, color='k', alpha=0.2)
    provinces_ae.boundary.plot(ax=axin, color='k', linestyle=':', alpha=0.1)
    graticules_ae.plot(ax=axin, color='grey', linestyle='--', alpha=0.5)
    # data
    DA.plot(ax=axin, vmin=0, vmax=15, cmap='Spectral_r')
    # labels
    plt.title("")
    plt.xlabel("")
    plt.ylabel("")
    axin.get_xaxis().set_ticks([-1800000,800000,3700000])
    axin.get_yaxis().set_ticks([1100000])
    axin.get_xaxis().set_ticklabels(["120°W","90°W","60°W"])
    axin.get_yaxis().set_ticklabels(["150°W"])
    axes2 = axin.secondary_xaxis('top')
    axes2.get_xaxis().set_ticks([3550000])
    axes2.get_xaxis().set_ticklabels(["60°N"])
    return fig

def make_plot2(DA, ticklabels, label):
    fig = plt.figure(figsize=(10,4))
    axin = plt.gca()
    plt.axis([-4500000,4500000,350000,4950000])
    # geographic layer
    countries_ae.loc[countries_ae['ADMIN']!='Canada'].plot(ax=axin, color='lightgrey')
    countries_ae.boundary.plot(ax=axin, color='k', alpha=0.2)
    provinces_ae.boundary.plot(ax=axin, color='k', linestyle=':', alpha=0.1)
    graticules_ae.plot(ax=axin, color='grey', linestyle='--',alpha=0.5)
    # data
    cax = DA.plot(ax=axin, vmin=0, vmax=2, cmap='coolwarm', add_colorbar=False)
    cbar = fig.colorbar(cax, ticks=[0,1,2], label=label)
    cbar.ax.set_yticklabels(ticklabels)
    # labels
    plt.title("")
    plt.xlabel("")
    plt.ylabel("")
    axin.get_xaxis().set_ticks([-1800000,800000,3700000])
    axin.get_yaxis().set_ticks([1100000])
    axin.get_xaxis().set_ticklabels(["120°W","90°W","60°W"])
    axin.get_yaxis().set_ticklabels(["150°W"])
    axes2 = axin.secondary_xaxis('top')
    axes2.get_xaxis().set_ticks([3550000])
    axes2.get_xaxis().set_ticklabels(["60°N"])
    return fig

def make_plot3(DA):
    fig = plt.figure(figsize=(10,4))
    axin = plt.gca()
    plt.axis([-4500000,4500000,350000,4950000])
    # geographic layer
    countries_ae.loc[countries_ae['ADMIN']!='Canada'].plot(ax=axin, color='lightgrey')
    countries_ae.boundary.plot(ax=axin, color='k', alpha=0.2)
    provinces_ae.boundary.plot(ax=axin, color='k', linestyle=':', alpha=0.1)
    graticules_ae.plot(ax=axin, color='grey', linestyle='--', alpha=0.5)
    # data
    DA.values = DA.values*100 # convert to percentage
    DA.plot(ax=axin, vmin=0, vmax=100, cmap='Spectral')
    # labels
    plt.title("")
    plt.xlabel("")
    plt.ylabel("")
    axin.get_xaxis().set_ticks([-1800000,800000,3700000])
    axin.get_yaxis().set_ticks([1100000])
    axin.get_xaxis().set_ticklabels(["120°W","90°W","60°W"])
    axin.get_yaxis().set_ticklabels(["150°W"])
    axes2 = axin.secondary_xaxis('top')
    axes2.get_xaxis().set_ticks([3550000])
    axes2.get_xaxis().set_ticklabels(["60°N"])
    return fig

def make_plot4(DA):
    fig = plt.figure(figsize=(10,4))
    axin = plt.gca()
    plt.axis([-4500000,4500000,350000,4950000])
    # geographic layer
    countries_ae.loc[countries_ae['ADMIN']!='Canada'].plot(ax=axin, color='lightgrey')
    countries_ae.boundary.plot(ax=axin, color='k', alpha=0.2)
    provinces_ae.boundary.plot(ax=axin, color='k', linestyle=':', alpha=0.1)
    graticules_ae.plot(ax=axin, color='grey', linestyle='--', alpha=0.5)
    # data
    DA.values = DA.values*100 # convert to percentage
    DA.plot(ax=axin, vmin=0, vmax=50, cmap='Spectral')
    # labels
    plt.title("")
    plt.xlabel("")
    plt.ylabel("")
    axin.get_xaxis().set_ticks([-1800000,800000,3700000])
    axin.get_yaxis().set_ticks([1100000])
    axin.get_xaxis().set_ticklabels(["120°W","90°W","60°W"])
    axin.get_yaxis().set_ticklabels(["150°W"])
    axes2 = axin.secondary_xaxis('top')
    axes2.get_xaxis().set_ticks([3550000])
    axes2.get_xaxis().set_ticklabels(["60°N"])
    return fig

# %% figure
fig2a = make_plot3(full_xr_ae.E_error)
fig2a.savefig(os.path.join(FIGS_PATH,"Figure2a_E_error.png"))

fig2b1 = make_plot1(full_xr_ae.E_A)
fig2b1.savefig(os.path.join(FIGS_PATH,"Figure2b_E_A.png"))

fig2b2 = make_plot1(full_xr_ae.V_A)
fig2b2.savefig(os.path.join(FIGS_PATH,"Figure2b_V_A.png"))

fig2c1 = make_plot3(full_xr_ae.E_P)
fig2c1.savefig(os.path.join(FIGS_PATH,"Figure2b_E_P.png"))

fig2c2 = make_plot4(full_xr_ae.V_P)
fig2c2.savefig(os.path.join(FIGS_PATH,"Figure2b_V_P.png"))

#fig2c = make_plot1(full_xr_ae.EVVE)
#fig2c.savefig(os.path.join(FIGS_PATH,"Figure2c_EVVE.png"))

fig2d = make_plot2(full_xr_ae.windcat,['Low','Medium','High'], 'Wind Speed Index')
fig2d.savefig(os.path.join(FIGS_PATH,"Figure2d_windcat.png"))

fig2e = make_plot2(full_xr_ae.phase, ['Solid','Mixed','Liquid'], "Precipitation Phase Index")
fig2e.savefig(os.path.join(FIGS_PATH,"Figure2e_phase.png"))

#fig2f = make_plot1(full_xr_ae.EE)
#fig2f.savefig(os.path.join(FIGS_PATH,"Figure2f_EE.png"))


fig2a = make_plot3(full_xr_ae.E_error_cool)
fig2a.savefig(os.path.join(FIGS_PATH,"Figure2a_E_error_cool.png"))

fig2b1 = make_plot1(full_xr_ae.E_A_cool)
fig2b1.savefig(os.path.join(FIGS_PATH,"Figure2b_E_A_cool.png"))

fig2b2 = make_plot1(full_xr_ae.V_A_cool)
fig2b2.savefig(os.path.join(FIGS_PATH,"Figure2b_V_A_cool.png"))

fig2c1 = make_plot3(full_xr_ae.E_P_cool)
fig2c1.savefig(os.path.join(FIGS_PATH,"Figure2b_E_P_cool.png"))

fig2c2 = make_plot4(full_xr_ae.V_P_cool)
fig2c2.savefig(os.path.join(FIGS_PATH,"Figure2b_V_P_cool.png"))

#fig2c = make_plot1(full_xr_ae.EVVE)
#fig2c.savefig(os.path.join(FIGS_PATH,"Figure2c_EVVE.png"))

fig2d = make_plot2(full_xr_ae.windcat_cool,['Low','Medium','High'], 'Wind Speed Index for Cool Season')
fig2d.savefig(os.path.join(FIGS_PATH,"Figure2d_windcat_cool.png"))

fig2e = make_plot2(full_xr_ae.phase_cool, ['Solid','Mixed','Liquid'], "Precipitation Phase Index for Cool Season")
fig2e.savefig(os.path.join(FIGS_PATH,"Figure2e_phase_cool.png"))


fig2a = make_plot3(full_xr_ae.E_error_warm)
fig2a.savefig(os.path.join(FIGS_PATH,"Figure2a_E_error_warm.png"))

fig2b1 = make_plot1(full_xr_ae.E_A_warm)
fig2b1.savefig(os.path.join(FIGS_PATH,"Figure2b_E_A_warm.png"))

fig2b2 = make_plot1(full_xr_ae.V_A_warm)
fig2b2.savefig(os.path.join(FIGS_PATH,"Figure2b_V_A_warm.png"))

fig2c1 = make_plot3(full_xr_ae.E_P_warm)
fig2c1.savefig(os.path.join(FIGS_PATH,"Figure2b_E_P_warm.png"))

fig2c2 = make_plot4(full_xr_ae.V_P_warm)
fig2c2.savefig(os.path.join(FIGS_PATH,"Figure2b_V_P_warm.png"))

#fig2c = make_plot1(full_xr_ae.EVVE)
#fig2c.savefig(os.path.join(FIGS_PATH,"Figure2c_EVVE.png"))

fig2d = make_plot2(full_xr_ae.windcat_warm,['Low','Medium','High'], 'Wind Speed Index for Warm Season')
fig2d.savefig(os.path.join(FIGS_PATH,"Figure2d_windcat_warm.png"))

fig2e = make_plot2(full_xr_ae.phase_warm, ['Solid','Mixed','Liquid'], "Precipitation Phase Index for Warm Season")
fig2e.savefig(os.path.join(FIGS_PATH,"Figure2e_phase_warm.png"))