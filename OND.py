#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Aug 12 11:50:31 2020

@author: dominique.brunet@ec.gc.ca
"""

from collections import namedtuple
import numpy as np
from scipy.stats import norm
from scipy.linalg import cholesky, cho_solve
from scipy.linalg import eig as eigendecomposition
from scipy.linalg import solve_triangular

import pickle

def assert_one_dimensional(x):
    assert np.prod(x.shape) == len(x)

def get_bounds(x):
    nx = len(x)
    if nx>1:
        xmin = np.minimum(x[0], x[-1])
        xmax = np.maximum(x[0], x[-1])
        dx = (xmax-xmin)/(nx-1)
        xmin = xmin-dx/2
        xmax = xmax+dx/2
    else:
        dx = 0
        xmin = np.minimum(x[0], x[-1])
        xmax = np.maximum(x[0], x[-1])
    return (nx, dx, xmin, xmax)

def get_bbox(grid):
    return (grid.xmin, grid.xmax, grid.ymin, grid.xmax)
    
def get_grid(*coords):
    grid = namedtuple('Grid', ['nx','dx','xmin','xmax',
                               'ny','dy','ymin','ymax',
                               'nz','dz','zmin','zmax'])
    dim = len(coords)
    if (dim<1) or (dim>3):
        raise ValueError("Between one and three dimensions required")
    if dim>=1:
        (nx, dx, xmin, xmax) = get_bounds(coords[0])
        grid.dx = dx
        grid.nx = nx
        grid.xmin = xmin
        grid.xmax = xmax
        grid.x = coords[0]
    if dim>=2:
        (ny, dy, ymin, ymax) = get_bounds(coords[1])
        grid.dy = dy
        grid.ny = ny
        grid.ymin = ymin
        grid.ymax = ymax
        grid.y = coords[1]
    if dim==3:
        (nz, dz, zmin, zmax) = get_bounds(coords[2])
        grid.dz = dz
        grid.nz = nz
        grid.zmin = zmin
        grid.zmax = zmax
        grid.z = coords[1]
    return grid

def get_grid_index(grid, bbox):
    # Get corresponding index bounds for bbox relative to grid
    i_min = np.maximum(0, np.floor((bbox.xmin - grid.xmin)/grid.dx))
    i_max = np.maximum(grid.ny-1, np.floor((bbox.xmax - grid.xmin)/grid.dx))
    j_min = np.minimum(0, np.floor((bbox.ymin - grid.ymin)/grid.dy))
    j_max = np.minimum(grid.nx-1, np.floor((bbox.xmax - grid.ymin)/grid.dy))
    # Create pairs of coordinates on grid
    i = np.arange(i_min, i_max+1).astype(int)
    j = np.arange(j_min, j_max+1).astype(int)
    I, J = np.meshgrid(i, j)
    # Transform to linear index
    index = np.unique(J+I*grid.nx)
    return index

def get_station_index(table, x0, y0, 
                      xname='Longitude', yname='Latitude', delta=0.1):
    coords = table[[xname,yname]].values
    j = np.floor((coords[:,0]-(x0 - delta/2))/delta).astype(int) # horizontal (x increasing)
    i = np.floor(((y0 + delta/2)-coords[:,1])/delta).astype(int) # vertical (y decreasing)
    return i,j

def compute_cross_corr_stats(X, Y):
    mu_x = np.nanmean(X, axis=0)
    mu_y = np.nanmean(Y, axis=0)
    sigma_x = np.nanstd(X, axis=0)
    sigma_y = np.nanstd(Y, axis=0)
    Sigma_xx = compute_cross_corr_mat(X, X)
    Sigma_xy = compute_cross_corr_mat(X, Y)
    Sigma_yy = compute_cross_corr_mat(Y, Y)
    return mu_x, mu_y, sigma_x, sigma_y, Sigma_xx, Sigma_xy, Sigma_yy

def compute_cross_corr_mat(Xi, Xj):
    nans_i = np.isnan(Xi)
    nans_j = np.isnan(Xj)
    Xi[nans_i] = 0
    Xj[nans_j] = 0
    not_nans_i = 1-nans_i.astype(float)
    not_nans_j = 1-nans_j.astype(float)
    nX = not_nans_i.T @ not_nans_j
    sum_Xi = (not_nans_j.T @ Xi).T
    sum_Xj = not_nans_i.T @ Xj
    sum_Xi2 = (not_nans_j.T @ Xi**2).T
    sum_Xj2 = not_nans_i.T @ Xj**2
    X0i = sum_Xi/nX
    X0j = sum_Xj/nX
    sXi = np.sqrt(sum_Xi2/nX-X0i**2)
    sXj = np.sqrt(sum_Xj2/nX-X0j**2)
    Sigma_ij = ((Xi.T @ Xj)/nX - X0i * X0j)/(sXi * sXj)
    Sigma_ij[np.isnan(Sigma_ij)] = 0 # assign zero correlation if variance is zero
    return Sigma_ij

def compute_posterior(mu_x, mu_y, Sigma_xx, Sigma_yy, Sigma_xy, y, sigma_n, 
                      mu_out=True, cov_out=True):
    """
    Parameters
    ----------
    mu_x : TYPE
        DESCRIPTION.
    mu_y : TYPE
        DESCRIPTION.
    Sigma_xx : TYPE
        DESCRIPTION.
    Sigma_yy : TYPE
        DESCRIPTION.
    Sigma_xy : TYPE
        DESCRIPTION.
    y : TYPE
        DESCRIPTION.
    sigma_n : TYPE
        DESCRIPTION.

    Returns
    -------
    mu_post : TYPE
        DESCRIPTION.
    Sigma_post : TYPE
        DESCRIPTION.
        
    See Rasmussen, C. E., & Williams, C. K. I. (2006).
    Gaussian processes for machine learning (Vol. 2, Number 3). 
    MIT Press Cambridge, MA.
    Algorithm 2.1
    and
    https://github.com/scikit-learn/scikit-learn/blob/0fb307bf3/sklearn/gaussian_process/_gpr.py#L23
    """
    assert Sigma_xx.shape == (len(mu_x), len(mu_x))
    assert Sigma_yy.shape == (len(mu_y), len(mu_y))
    assert Sigma_xy.shape == (len(mu_x), len(mu_y))
    lower = True
    L = cholesky(Sigma_yy+np.eye(len(Sigma_yy))*sigma_n**2, lower=lower)
    v = cho_solve((L, lower), Sigma_xy.T)
    if mu_out:
        assert y.shape[0] == len(mu_y)
        assert_one_dimensional(mu_x)
        assert_one_dimensional(mu_y)
        mu_x = mu_x.reshape(-1,1)
        mu_y = mu_y.reshape(-1,1)
        [N, nt] = y.shape
        mu_y_copy = np.repeat(mu_y, nt, axis=1)
        y[np.isnan(y)] = mu_y_copy[np.isnan(y)] # consider no precip as trace precip amount
        mu_post = mu_x + v.T @ (y-mu_y)
    else:
        mu_post = mu_x
    if cov_out:
        Sigma_post = Sigma_xx - Sigma_xy @ v
    else:
        alpha = L.T @ v
        Sigma_post = np.diag(Sigma_xx) - np.sum(alpha * alpha, axis=0)
    return mu_post, Sigma_post

def update_L(L_N, Sigma_yy, sigma_n):
    sigma_yy_N = Sigma_yy[:-1,-1]
    sigma_yy_i = Sigma_yy[-1,-1]
    lower = True
    l_N = solve_triangular(L_N, sigma_yy_N, lower=lower)
    l_i = np.sqrt(sigma_yy_i + sigma_n[-1]**2 - l_N.T @ l_N)
    l = np.concatenate((l_N.reshape(1,-1), l_i.reshape(1,1)), axis=1)
    L_N = np.concatenate((L_N, np.zeros((len(l_N), 1))), axis=1)
    L = np.concatenate((L_N, l.reshape(1,-1)), axis=0)
    return L

def update_v(v_N, L, Sigma_xy):
    l_N = L[:-1,-1]
    l_i = L[-1,-1]
    sigma_xy_i = Sigma_xy[:,-1]
    v_i = (sigma_xy_i.T - l_N.T @ v_N)/l_i
    v = np.concatenate((v_N, v_i.reshape(1,-1)), axis=0)
    return v

def update_alpha(alpha_N, L, v):
    alpha_i = L[:,-1].T @ v
    alpha = np.concatenate((alpha_N, alpha_i.reshape(1,-1)), axis=0)
    return alpha

def update_sigma_yy(Sigma_yy, y, sigma_y):
    y_N = y[:-1,:]
    y_i = y[-1,:].reshape(1,-1)
    sigma_y_N = sigma_y[:-1]
    sigma_y_i = sigma_y[-1]
    corr_yy_N = compute_cross_corr_mat(y_N.T, y_i.T)
    sigma_yy_N = sigma_y_N.reshape(-1,1) @ sigma_y_i.reshape(1,-1) * corr_yy_N.reshape(-1,1)
    sigma_yy_i = sigma_y_i**2
    Sigma_yy_N = np.concatenate((Sigma_yy, sigma_yy_N), axis=1)
    Sigma_yy_i = np.concatenate((sigma_yy_N.T, sigma_yy_i.reshape(1,1)), axis=1)
    Sigma_yy = np.concatenate((Sigma_yy_N, Sigma_yy_i), axis=0)
    return Sigma_yy

def update_sigma_xy(Sigma_xy_N, X, i, sigma_x):
    sigma_y = sigma_x[i]
    corr_xy_i = compute_cross_corr_mat(X, X[:,i])
    sigma_xy_i = sigma_x.reshape(-1,1) @ sigma_y.reshape(1,-1) * corr_xy_i.reshape(-1,1)
    Sigma_xy = np.concatenate((Sigma_xy_N, sigma_xy_i), axis=1)
    return Sigma_xy

def compute_posterior_mean(mu_x, mu_y, v, y):
    mu_post = mu_x + v.T @ (y-mu_y)
    return mu_post

def compute_posterior_variance(sigma2_x, alpha):
    sigma2_post = sigma2_x - np.sum(alpha * alpha, axis=0)
    return sigma2_post

def compute_posterior_faster(mu_x, mu_y, sigma_x, Sigma_yy, Sigma_xy, y, sigma_n, L_N):
    L = update_L(L_N, Sigma_yy, sigma_n)
    #assert np.max(np.abs((L@L.T)-Sigma_yy-np.eye(len(Sigma_yy))*sigma_n**2))<1e-4
    lower = True
    v = cho_solve((L, lower), Sigma_xy.T)
    assert np.max(np.abs((L@L.T)@v - Sigma_xy.T))<1e-4
    # Mean
    assert y.shape[0] == len(mu_y)
    assert_one_dimensional(mu_x)
    assert_one_dimensional(mu_y)
    mu_x = mu_x.reshape(-1,1)
    mu_y = mu_y.reshape(-1,1)
    [N, nt] = y.shape
    mu_y_copy = np.repeat(mu_y, nt, axis=1)
    y[np.isnan(y)] = mu_y_copy[np.isnan(y)] # consider no precip as trace precip amount
    mu_post = mu_x + v.T @ (y-mu_y)
    # Covariance
    alpha = L.T @ v
    sigma2_post = sigma_x**2 - np.sum(alpha * alpha, axis=0)
    return mu_post, sigma2_post, L


def compute_posterior_fast(mu_x, mu_y, sigma_x, Sigma_yy, Sigma_xy, y, sigma_n):
    """
    Parameters
    ----------
    mu_x : TYPE
        DESCRIPTION.
    mu_y : TYPE
        DESCRIPTION.
    Sigma_xx : TYPE
        DESCRIPTION.
    Sigma_yy : TYPE
        DESCRIPTION.
    Sigma_xy : TYPE
        DESCRIPTION.
    y : TYPE
        DESCRIPTION.
    sigma_n : TYPE
        DESCRIPTION.

    Returns
    -------
    mu_post : TYPE
        DESCRIPTION.
    Sigma_post : TYPE
        DESCRIPTION.
        
    See Rasmussen, C. E., & Williams, C. K. I. (2006).
    Gaussian processes for machine learning (Vol. 2, Number 3). 
    MIT Press Cambridge, MA.
    Algorithm 2.1
    and
    https://github.com/scikit-learn/scikit-learn/blob/0fb307bf3/sklearn/gaussian_process/_gpr.py#L23
    """
    assert Sigma_yy.shape == (len(mu_y), len(mu_y))
    assert Sigma_xy.shape == (len(mu_x), len(mu_y))
    lower = True
    assert Sigma_yy.shape == (np.eye(len(Sigma_yy))*sigma_n**2).shape
    L = cholesky(Sigma_yy+np.eye(len(Sigma_yy))*sigma_n**2, lower=lower)
    assert Sigma_xy.T.shape[0] == L.shape[0]
    with open('data.pickle', 'wb') as f:
        pickle.dump({'L':L, 'Sigma_xy':Sigma_xy, 'Sigma_yy':Sigma_yy, 'sigma_n':sigma_n}, f, pickle.HIGHEST_PROTOCOL)
    v = cho_solve((L, lower), Sigma_xy.T)
    # Mean
    assert y.shape[0] == len(mu_y)
    assert_one_dimensional(mu_x)
    assert_one_dimensional(mu_y)
    mu_x = mu_x.reshape(-1,1)
    mu_y = mu_y.reshape(-1,1)
    [N, nt] = y.shape
    mu_y_copy = np.repeat(mu_y, nt, axis=1)
    y[np.isnan(y)] = mu_y_copy[np.isnan(y)] # consider no precip as trace precip amount
    mu_post = mu_x + v.T @ (y-mu_y)
    # Covariance
    alpha = L.T @ v
    sigma2_post = sigma_x**2 - np.sum(alpha * alpha, axis=0)
    return mu_post, sigma2_post, L

def sample_multivariate(mu, Sigma, m=100):
    # Inefficient method, should decompose Sigma = LL.T once and sample from there
    #assert Sigma.shape == (len(mu), len(mu))
    #assert_one_dimensional(mu)
    N, nt = mu.shape
    mu = mu.reshape(N, nt, 1)
    mu = np.repeat(mu, m, axis=2)
    lower = True
    L = cholesky(Sigma+np.eye(len(Sigma))*0.0001**2, lower=lower)
    u = np.random.normal(loc=0, scale=1, size=N*m).reshape(N, m)
    Lu = L @ u
    Lu = Lu.reshape(N, 1, m)
    Lu = np.repeat(Lu, nt, axis=1)    
    return mu + Lu 

def compute_POP_threshold(mu, sigma):
    """
    See: https://en.wikipedia.org/wiki/Truncated_normal_distribution
    """
    if mu.ndim>1:
        sigma = sigma.reshape(-1,1)
    
    rv = norm()
    # alpha and beta are the normalized values of the normal distribution at,
    # respectively, 0 and 1
    alpha = -mu/sigma
    beta = (1-mu)/sigma
    # Z is the probability that samples are within the [0,1] range
    Z = rv.cdf(beta) - rv.cdf(alpha)
    # POP is the expectation of the truncated [0,1] normal distribution
    # with mean mu and standard deviation sigma
    # Set to 0 or 1 if Z very small
    POP = np.where(Z>0.0001, 
                   mu + (rv.pdf(alpha)-rv.pdf(beta))/Z*sigma,
                   mu)
    POP = np.maximum(0, np.minimum(1, POP))
    # Threshold is the value over which precipitation events are occurring
    threshold = mu + sigma*rv.ppf(1-POP)
    return threshold, POP

def make_SPD(Sigma):
    Y = (Sigma+Sigma.T)/2 # make symmetric
    d, Q = eigendecomposition(Y, left=True, right=False)
    D = np.diag(np.real(d))
    D_plus = np.maximum(0, D)
    Z = (Q @ D_plus) @ Q.T
    return np.real(Z)
    
def aggregate_uncertainty(sigmas):
    sigmas = np.array(sigmas)
    return np.sum(sigmas**2, axis=-1) # squared uncertainty
