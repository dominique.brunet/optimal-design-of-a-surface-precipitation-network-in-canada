# Optimal Design Of A Surface Precipitation Network In Canada

Code to reproduce results for "Brunet, D., and J. A. Milbrandt, 2023: Optimal Design of a Surface Precipitation Network in Canada. J. Hydrometeor., https://doi.org/10.1175/JHM-D-22-0085.1"

## Installation

Use the following command to install all the packages needed. (Prior installation of Anaconda Python or Miniconda required, see e.g. https://docs.conda.io/projects/conda/en/stable/user-guide/install/download.html#anaconda-or-miniconda)

`conda create --name OND -c conda-forge numpy scipy matplotlib pandas geopandas pyproj shapely xarray netcdf4 jupyter cdsapi python=3.7`

## Source Data
The DATA/SOURCE folder needs to be populated from online sources using Python scripts or manual downloads:
- 'SPICE/post-spice_data.csv' is downloaded from https://doi.org/10.1594/PANGAEA.907379 using get_spice_data.py
- 'Stations/SCDNA/SCDNA_v1.1.nc4' is downloaded from https://doi.org/10.5281/zenodo.3953310 using get_scdna_data.py
- 'ERA5-Land/\*/\*.nc' data is downloaded from the Copernicus Datastore using get_era5-land_data.py
A free account on the Copernicus Climate Data Store (https://cds.climate.copernicus.eu/) is required. The download keys found under user needs to be stored in .cdsapirc in the home directory
- 'Geographical/NaturalEarth/\*.zip' is (manually) downloaded from Natural Earth Data (https://www.naturalearthdata.com/). Automated download failed, but paths and folder creation are in get_geographical_data.py.

## Preprocessed Data
The DATA/PREPROCESSED folder contains pre-processed source data using Python scripts:
- 'SPICE/daily_post-spice_data.csv' was processed using preprocess_spice_data.py.
- 'Stations/SCDNA/SCDNA_stations.\*' metadata was processed using preprocess_scdna_data.py. 
The station data 'Stations/SCDNA/SCDNA_data.nc' is also processed with the same script but only used in the paper to validate if two stations are duplicated. The SCDNA data is generated from the script but not included directly in the repository. 
- 'ERA5-Land/\*/\*.nc' data are processed using preprocess_era5-land_data.py
- 'Geographical/geographical_mask.nc' and 'Geographical/Provinces-States.\*' are processed using preprocess_geographical_data.py.

## Other scripts
Other scripts used are added here, but still require clean-up and documentation.
