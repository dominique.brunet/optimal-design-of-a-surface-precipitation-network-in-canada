# -*- coding: utf-8 -*-
"""
Created on Tue Nov 29 10:24:04 2022

@author: BrunetD
"""

import os
import urllib

DATA_PATH = 'DATA'

# %% Data downloaded from https://doi.org/10.5281/zenodo.3953310
filename = "https://zenodo.org/record/3953310/files/SCDNA_v1.1.nc4"

dest_folder = os.path.join(DATA_PATH, "SOURCE", "Stations", "SCDNA")
os.makedirs(dest_folder, exist_ok=True)
dest_path = os.path.join(dest_folder, filename.split('/')[-1])
urllib.request.urlretrieve(filename, dest_path)
