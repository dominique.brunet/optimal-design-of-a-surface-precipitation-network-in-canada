#!/usr/bin/env python
# coding: utf-8

# Modules
#from datetime import datetime
import numpy as np
import os
import pandas as pd
import xarray as xr
#from scipy.stats import norm
#from PIL import ImageDraw, Image
#from shapely.ops import transform
#import sys

from collections import Counter

#from mpl_toolkits.axes_grid1.inset_locator import inset_axes
import shapely.geometry as sg # Operations on shapes (points, lines and polygons)
from pyproj import CRS # Coordinate Reference System (map projections)
import geopandas as gpd # Geospatial extension of Pandas
 
import OND

from matplotlib import pyplot as plt
#import matplotlib.cm as cm
#from matplotlib.colors import LogNorm

from csv import writer

# # Plotting
def plot_uncertainty(U, j_max, i_max, j_max_all, i_max_all, NA_gdf, name, latitude, longitude, vmin=0, vmax=10):
    fig = plt.figure(figsize=(15,15))
    ax = plt.gca()
    
    region_no = NA_gdf[NA_gdf['Name']==name].index[0]
    region = NA_gdf.iloc[region_no]

    (lon_min1, lat_min1, lon_max1, lat_max1) = region['geometry'].bounds
    ax.axis([lon_min1-1, lon_max1+1, lat_min1-1, lat_max1+1])

    
    #station_select.plot(ax=ax, marker='.', zorder=9, alpha=0.8, color='r')
    if (i_max is not None) & (j_max is not None):
        ax.plot(longitude[j_max], latitude[i_max], 'xr', zorder=10)
    if (len(i_max_all)>0) & (len(j_max_all)>0):
        ax.plot(longitude[j_max_all], latitude[i_max_all], '.r', zorder=10)
    #countries.plot(ax=ax, color='lightgrey', zorder=-1)
    #countries.loc[countries['ADMIN']=='Canada'].boundary.plot(ax=ax, color='dimgrey')
    NA_gdf.plot(ax=ax, color='lightgrey', linestyle='--', edgecolor='k')
    
    NA_gdf.loc[region_no-1:region_no].plot(ax=ax, color='none', linestyle='--', edgecolor='r', zorder=9)
    h = ax.imshow(U, 
                  vmin=vmin, vmax=vmax, 
                  extent=[lon_min-delta/2, lon_max+delta/2, lat_min-delta/2, lat_max+delta/2], 
                  zorder=2)
    cax = fig.add_axes([0.24, 0.15, 0.545, 0.025])
    fig.colorbar(h, cax=cax, orientation='horizontal')
    plt.title(region['Name'])
    #provinces.loc[provinces['name']=='British Columbia'].boundary.plot(ax=ax, color='dimgrey', linestyle='--')
    #provinces.loc[provinces['name']=='Saskatchewan'].boundary.plot(ax=ax, color='dimgrey', linestyle='--')
    #ax.text(-115.5, 56.75, 'Alberta', color='dimgrey', fontsize=24)
    #ax.text(-117.75, 51.25, 'B.C.', fontsize=24, color='dimgrey')
    return fig


def vec2im(vec, nx, ny, mask):
    im = np.zeros((ny,nx))
    mask = mask.reshape(ny,nx)
    im[mask] = np.nan
    im[~mask] = vec.squeeze()
    return im

# In[15]:
def combine_uncertainty(mu_A_x, sigma_A_x, POP_prior, sigma_A_n):
    if len(mu_A_x.shape)==2:
        sigma_A_n = sigma_A_n.reshape(-1,1) # for broadcasting along time
    E_A = np.exp(mu_A_x + sigma_A_x**2/2)
    #E_AM = np.exp(sigma_A_n**2/2)*E_A
    #E_TP = E_amount*POP_prior
    V_POP = np.maximum(0, POP_prior*(1-POP_prior))
    V_A = (np.exp(sigma_A_x**2)-1)*np.exp(2*mu_A_x+sigma_A_x**2)
    #V_AM = (np.exp(sigma_A_n**2)-1)*np.exp(sigma_A_n**2)*E_A**2\
    #        + np.exp(2*sigma_A_n**2)*V_A
    V_TP = POP_prior*V_A+V_POP*E_A**2
    return V_TP
    
def count_multiple(s):
    counts = Counter(s)
    multiplicity = [counts[elem] for elem in s]
    return np.array(multiplicity)
   

def compute_uncertainty(s, X_POP, mu_POP_x, sigma_POP_x, L_POP, X, mu_A_x, sigma_A_x, L_A, sigma_POP_n, sigma_A_n):
    
    # Update y
    y_POP = X_POP[:,s]
    y_A = X[:,s]
    
    # covariance matrices
    mu_POP_y = mu_POP_x[s]
    sigma_POP_y = sigma_POP_x[s]

    Corr_POP_xy = OND.compute_cross_corr_mat(X_POP, y_POP)
    Sigma_POP_xy = sigma_POP_x.reshape(-1,1) @ sigma_POP_y.reshape(1,-1) * Corr_POP_xy

    Corr_POP_yy = OND.compute_cross_corr_mat(y_POP, y_POP)
    Sigma_POP_yy = sigma_POP_y.reshape(-1,1) @ sigma_POP_y.reshape(1,-1) * Corr_POP_yy

    mu_A_y = mu_A_x[s]
    sigma_A_y = sigma_A_x[s]

    Corr_A_xy = OND.compute_cross_corr_mat(X, y_A)
    Sigma_A_xy = sigma_A_x.reshape(-1,1) @ sigma_A_y.reshape(1,-1) * Corr_A_xy

    Corr_A_yy = OND.compute_cross_corr_mat(y_A, y_A)
    Sigma_A_yy = sigma_A_y.reshape(-1,1) @ sigma_A_y.reshape(1,-1) * Corr_A_yy

    ### Posterior
    if L_POP is None:
        #L_POP = np.sqrt(Sigma_POP_yy+sigma_POP_n**2)
        mu_POP_post, sigma_POP_post, L_POP = OND.compute_posterior_fast(mu_POP_x, 
                                                         mu_POP_y, 
                                                         sigma_POP_x, 
                                                         Sigma_POP_yy,
                                                         Sigma_POP_xy, 
                                                         y_POP.T, 
                                                         sigma_POP_n)
    else:
        mu_POP_post, sigma_POP_post, L_POP = OND.compute_posterior_faster(mu_POP_x, 
                                                                      mu_POP_y, 
                                                                      sigma_POP_x, 
                                                                      Sigma_POP_yy, 
                                                                      Sigma_POP_xy, 
                                                                      y_POP.T, 
                                                                      sigma_POP_n, 
                                                                      L_POP)
    sigma_POP_post = np.sqrt(sigma_POP_post).reshape(-1,1)
    
    # POP and threshold
    threshold_POP_post, POP_post = OND.compute_POP_threshold(mu_POP_post, sigma_POP_post)
    
    multiplicity = count_multiple(s)
    sigma_e = sigma_A_n.copy()*np.ones(mu_A_x.shape[0])
    sigma_e[s] = sigma_e[s]/np.sqrt(multiplicity) # adjust meas. unc. for multiple stations in same grid cell
    
    if L_A is None:
        #L_A = np.sqrt(Sigma_A_yy+sigma_A_n**2)
        mu_A_post, sigma_A_post, L_A = OND.compute_posterior_fast(mu_A_x, 
                                                    mu_A_y, 
                                                    sigma_A_x, 
                                                    Sigma_A_yy, 
                                                    Sigma_A_xy, 
                                                    y_A.T, 
                                                    sigma_e[s])
    else:
        mu_A_post, sigma_A_post, L_A = OND.compute_posterior_faster(mu_A_x, 
                                                        mu_A_y, 
                                                        sigma_A_x, 
                                                        Sigma_A_yy, 
                                                        Sigma_A_xy,
                                                        y_A.T, 
                                                        sigma_e[s], 
                                                        L_A)
    sigma_A_post = np.sqrt(sigma_A_post).reshape(-1,1)

    # Uncertainty
    #E_amount = np.exp(mu_A_post + sigma_A_post**2/2)
    #V_amount = (np.exp(sigma_A_post**2)-1)*np.exp(2*mu_A_post+sigma_A_post**2)
    #uncertainty_TP = POP_post*V_amount+POP_post*(1-POP_post)*E_amount**2
    uncertainty_TP = combine_uncertainty(mu_A_post, sigma_A_post, POP_post, sigma_A_n)
    uncertainty_TP_agg = np.mean(uncertainty_TP, axis=1)
    return uncertainty_TP_agg, L_POP, L_A

def write_old(file_name, row, mode='a+'):
    """
    CSV write function to append one row to file_name

    Note that the file is opened and closed each time this function is called to ensure that data is written on the disk. This behavior is to make sure that intermediate data is saved even if a hard-crash occurs.
    """
    with open(file_name, mode, newline='') as write_obj:
        csv_writer = writer(write_obj)
        csv_writer.writerow(row)
    return None


def write(file_name, row, mode='a+'):
    """
    CSV write function to append one row to file_name

    Note that the file is opened and closed each time this function is called to ensure that data is written on the disk. This behavior is to make sure that intermediate data is saved even if a hard-crash occurs.
    """
    with open(file_name, mode) as f:
        f.write(str(row[0])+","+str(row[1])+","+str(row[2])+"\n")        
    return None


def compute_prior(X_A, X_POP, sigma_A_n=0):
   sigma_A_n2 = sigma_A_n**2
   mu_P_x = X_POP.mean(dim='time').values
   sigma_P_x2 = X_POP.var(dim='time').values
   V_AM = X_A.var(dim='time').values
   E_AM = X_A.mean(dim='time').values
   E_A = E_AM/np.exp(sigma_A_n2/2)
   V_A = V_AM/np.exp(2*sigma_A_n2)-E_AM**2*(np.exp(sigma_A_n2)-1)/np.exp(2*sigma_A_n2)
   sigma_A_x2 = np.log(V_A/E_A**2+1)
   mu_A_x = np.log(E_A) - sigma_A_x2/2
   sigma_A_x = np.sqrt(np.maximum(0, sigma_A_x2))
   sigma_P_x = np.sqrt(np.maximum(0, sigma_P_x2))
   return mu_A_x, sigma_A_x, mu_P_x, sigma_P_x
   

   
   
# In[16]:
def design_network_with_error(s, 
                   area_of_interest, 
                   #inclusion_zone, 
                   X, 
                   X_POP,
                   N, 
                   NA_gdf,
                   name,
                   measunc='uniform',
                   sigma_POP_n = np.array([0.01]), 
                   sigma_A_n = np.array([0.05])):
    #X_POP = (X>=T).astype(float)
    #X = np.where(X_POP, np.log(X), np.log(T))
    # Prior
    #sigma_POP_x = np.std(X_POP, axis=0)+0.01
    #mu_POP_x = np.mean(X_POP, axis=0)
    #sigma_A_x = np.nanstd(X, axis=0)+0.01
    #mu_A_x = np.nanmean(X, axis=0)
    
    mu_A_x, sigma_A_x, mu_POP_x, sigma_POP_x = compute_prior(X, X_POP, sigma_A_n)
    X = np.log(X.values) # assert X > 0
    X_POP = X_POP.values


    # initialization
    [ni,nj] = area_of_interest.shape
    area_of_interest = area_of_interest.ravel()
    original_idx = np.arange(0, len(area_of_interest))
    original_idx = original_idx[area_of_interest]#[inclusion_zone]
    area_idx = np.arange(0, len(original_idx))
    idx_dict = {origin:new for (origin,new) in zip(original_idx, area_idx)}

    if len(s)>0:
        s = set(s)
        s = s.intersection(set(original_idx))
        s = list(s)
        idx_list = [idx_dict[sidx] for sidx in s]
        network = 'SCDNA'
    else:
        network = 'scratch'
    N0 = len(s)
    #latitude = np.linspace(lat_max, lat_min, ni)
    #longitude = np.linspace(lon_min, lon_max, nj)
    file_name = os.path.join(TMP, f"NetworkDesign-{network}-{name.replace(' ','_')}-noseason-{measunc}.csv")
    try:
        print(f"Loading file {file_name}")
        df = pd.read_csv(file_name, index_col=0)
        s = list(df['Index'].loc[df["Index"].notnull()].values)
        idx_list = [idx_dict[sidx] for sidx in s]
    except OSError:
        print(f"No file found! Creating new file {file_name}")
        row = ['','Index','Uncertainty']
        print(row)
        write(file_name, row, mode='w')
    # initial optimal location
    N1 = len(s)
    print(N1)
    if (N0==0)&(N1==0):
            # POP
            threshold_POP_prior, POP_prior = OND.compute_POP_threshold(mu_POP_x, sigma_POP_x)
            ## Stats
            #E_A = np.exp(mu_A_x + sigma_A_x**2/2)
            #E_AM = np.exp(sigma_A_n**2/2)*E_A
            ##E_TP = E_amount*POP_prior
            #V_POP = np.maximum(0, POP_prior*(1-POP_prior))
            #V_A = (np.exp(sigma_A_x**2)-1)*np.exp(2*mu_A_x+sigma_A_x**2)
            #V_AM = (np.exp(sigma_A_n**2)-1)*np.exp(sigma_A_n**2)*E_A**2\
            #        + np.exp(2*sigma_A_n**2)*V_A
            #V_TP = POP_prior*V_AM+V_POP*E_AM**2
            V_TP = combine_uncertainty(mu_A_x, sigma_A_x, POP_prior, sigma_A_n)
            
            uncertainty_samples = [np.sqrt(np.max(V_TP))]
            row=[0,'',uncertainty_samples[-1]]
            print(row)
            #write(file_name, row)
            N1 = 1
            #U_TP = np.zeros((ni*nj,))*np.nan
            #U_TP[original_idx] = V_TP
            #U_TP = U_TP.reshape(ni,nj)
            #fig = plot_uncertainty(U_TP, None, None, [], [], NA_gdf, name, latitude, longitude, 
            #       vmin=0, vmax=20)
            #plt.show()              
            #fig.savefig(os.path.join(FIG,'OND-Experiment1-20201020',f'From_{network}',f"{name.replace(' ','_')}{0}.png"))
            #plt.close()
            
            idx = np.argmax(V_TP)#[inclusion_zone])
            idx_list = [idx]
            s = original_idx[idx_list]
            s = list(s)
            sidx = s[-1]
            i_max = (sidx // nj)
            j_max = (sidx % nj)
            i_max_all = [i_max]
            j_max_all = [j_max]
            #print(f"{N0}: {uncertainty_samples[-1]}")
    elif (N0>0)&(N0==N1):
            uncertainty_samples = [np.nan]*N0
            i_max_all = [sidx // nj for sidx in s]
            j_max_all = [sidx % nj for sidx in s]
    else:
            uncertainty_samples = list(df['Uncertainty'].values)
            i_max_all = [sidx // nj for sidx in s]
            j_max_all = [sidx % nj for sidx in s]
    uncertainty_TP_agg, L_POP, L_A = compute_uncertainty(idx_list, X_POP, mu_POP_x, sigma_POP_x, None, 
                                             X, mu_A_x, sigma_A_x, None, sigma_POP_n, sigma_A_n)
    uncertainty_samples.append(np.sqrt(np.max(uncertainty_TP_agg)))
    
    #U_TP = np.zeros((ni*nj,))*np.nan
    #U_TP[original_idx] = uncertainty_TP_agg
    #U_TP = U_TP.reshape(ni,nj)
    if (N0==0)&(N1==1):
            row=[1,s[-1],uncertainty_samples[-1]]
            print(row)
            write(file_name, row)
            #fig = plot_uncertainty(U_TP, j_max, i_max, j_max_all, i_max_all, NA_gdf, name, latitude, longitude, 
            #               vmin=0, vmax=20)
            #plt.show()
            k=1
            #fig.savefig(os.path.join(FIG,'OND-Experiment1-20201020',f'From_{network}',f"{name.replace(' ','_')}{k}.png"))
            #plt.close()
    elif (N0>0)&(N1==N0):
            for (k,sidx) in enumerate(s[:-1]):
                row=[k+1, sidx, '']
                print(row)
                write(file_name, row)
            k=N0
            row=[k,s[-1],uncertainty_samples[-1]]
            print(row)
            write(file_name, row)
            #fig = plot_uncertainty(U_TP, None, None, j_max_all, i_max_all, NA_gdf, name, latitude, longitude, 
            #       vmin=0, vmax=20)
            #plt.show()              
            #fig.savefig(os.path.join(FIG,'OND-Experiment1-20201020',f'From_{network}',f"{name.replace(' ','_')}{N0}.png"))
            #plt.close()
    else:
            k=N1
    #print(f"{k}: {uncertainty_samples[-1]}")
        
    # loop
    number_stations = list(range(k+1, N+1))
    for k in number_stations:
            # Sample
            idx = np.argmax(uncertainty_TP_agg)
            sidx = original_idx[idx]
            i_max = (sidx // nj)
            j_max = (sidx % nj)

            # Add
            idx_list.append(idx)
            s.append(sidx)
            i_max_all.append(i_max)
            j_max_all.append(j_max)

            uncertainty_TP_agg, L_POP, L_A = compute_uncertainty(idx_list, X_POP, mu_POP_x, sigma_POP_x, L_POP, 
                                                 X, mu_A_x, sigma_A_x, L_A, sigma_POP_n, sigma_A_n)
            uncertainty_samples.append(np.sqrt(np.max(uncertainty_TP_agg)))
            #print(f"{k}: {uncertainty_samples[-1]}")
            row=[k,sidx,uncertainty_samples[-1]]
            print(row)
            write(file_name, row)
            # Save uncertainty map
            #U_TP = np.zeros((ni*nj,))*np.nan
            #U_TP[original_idx] = uncertainty_TP_agg
            #U_TP = U_TP.reshape(ni,nj)
            #fig = plot_uncertainty(U_TP, j_max, i_max, j_max_all, i_max_all, NA_gdf, name, latitude, longitude, 
            #                   vmin=0, vmax=20)
            #plt.show()              
            #fig.savefig(os.path.join(FIG,'OND-Experiment1-20201020',f'From_{network}',f"{name.replace(' ','_')}{k+1}.png"))
            #plt.close()
    return uncertainty_samples

# Map projection
ll_crs = CRS.from_proj4("+proj=longlat +ellps=WGS84 +datum=WGS84 +no_defs")
def get_gdf(df, y='Latitude', x='Longitude', crs=ll_crs):
    df['geometry'] = [sg.Point(x,y) for (x,y) in df[[x, y]].values] # Iterate over coordinates, construct Shapely Point
    gdf = gpd.GeoDataFrame(df) # Make Pandas DataFrame a GeoPandas GeoDataFrame
    gdf.crs = crs # Initialize coordinate reference system (crs)
    return gdf

if __name__ == '__main__': 
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("-p", "--provinces", nargs='?', default='all') # provinces
    parser.add_argument("-n", "--network", nargs='?', default='scratch') # network
    parser.add_argument("-a", "--add", nargs='?', default='default') # add N stations
    parser.add_argument("-m", "--measurement", nargs='?', default='variable') # 'variable' or 'uniform' meas. unc.
    args = parser.parse_args()

    print(args)
    
    # Paths
    FIG = "FIG"
    TMP = os.path.join("DATA","RESULTS")
    OUT = TMP
    DATA_PATH = os.path.join("DATA","PREPROCESSED")
    # ### Spatio-temporal window selection

    # This is the gridded part
    lon_min = -170
    lon_max = -50
    lat_min = 40
    lat_max = 85

    # Resolution
    delta = 0.1

    # Size
    ni = int(np.round((lat_max-lat_min)/delta)+1)
    nj = int(np.round((lon_max-lon_min)/delta)+1)

    # This is the months and years used for both the gridded and station data
    #month_start = 1
    #month_end = 12
    #year_start = 2000
    #year_end = 2020

    # ## Import province mask
    mask_xr = xr.open_dataset(os.path.join(TMP, 'geographic_mask.nc'))

    # # Import geodataframe
    NA_gdf = gpd.read_file(os.path.join(TMP ,'Provinces-States.shp'))

    # ## Get SCDNA data
    # Data downloaded on Zenodo..
    scdna_file = os.path.join(DATA_PATH, "Stations", "SCDNA", "SCDNA_v1.1.nc4")
    
    # xarray is used to open NetCDF file
    SCDNA = xr.open_dataset(scdna_file)
    
    # IDs are formatted as strings
    IDs = SCDNA.ID.values.astype(str)
    sources = [c0+c1 for (c0,c1) in zip(IDs[0,:],IDs[1,:])]
    stationid = [c2+c3+c4+c5+c6+c7+c8+c9+c10+c11+c12 for (c2,c3,c4,c5,c6,c7,c8,c9,c10,c11,c12)\
               in zip(IDs[2,:],IDs[3,:],IDs[4,:],IDs[5,:],IDs[6,:],IDs[7,:],IDs[8,:],IDs[9,:],IDs[10,:],\
                      IDs[11,:],IDs[12,:])]
    
    # Only stations with precip data are kept
    precip_stations = ~np.isnan(SCDNA.prcp.values[:,0])
    
    # A meta-data table is created for station data
    station_table = pd.DataFrame(data={'Source':sources,
                                       'ID':stationid,
                                       'Latitude':SCDNA.LLE.values[0,:],
                                       'Longitude':SCDNA.LLE.values[1,:],
                                       'Elevation':SCDNA.LLE.values[2,:]})
    # OID is the Original ID used in SCDNA dataset
    station_table['OID'] = station_table['Source'] + station_table['ID']
    # Filter meta-data to only include stations with precip data
    precip_station_table = station_table[precip_stations]
    
    location_select_station = (precip_station_table['Latitude']>=lat_min-delta/2)&\
                (precip_station_table['Latitude']<lat_max+delta/2)&\
                (precip_station_table['Longitude']>=lon_min-delta/2)&\
                (precip_station_table['Longitude']<lon_max+delta/2)

    station_select = get_gdf(precip_station_table.loc[location_select_station])
    
    (i,j) = OND.get_station_index(station_select, lon_min, lat_max)

    index = i*nj+j # linear index
    index = np.unique(index)

    i_max_all = list(index // nj)
    j_max_all = list(index % nj)

    s = list(index)
    n_stations_SCDNA = len(s) # unique stations by pixel
    

    #PROGRAM_PATH = r"C:\\Users\brunetd\Desktop\PythonProjects\OND\optimal-design-of-surface-precipitation-network-in-canada-old\R1"
    #p_file = 'ERA5-Land_tp_NA40N_daily_aggregated.nc'
    #tp_agg = xr.open_dataset(os.path.join(PROGRAM_PATH, 'RESULTS', tp_file))
    #sigma_A_n = np.sqrt(tp_agg.EV.values.ravel()) # assume constant measurement uncertainty over time
    

    #network = 'scratch' # OVERWRITE

    if args.provinces == 'all':
        provinces = NA_gdf.iloc[:13]["Name"]
    else:
        try:
            region_no = int(args.provinces)
            provinces = NA_gdf.iloc[region_no:region_no+1]["Name"]
        except ValueError:
            provinces = [args.p]
    
    network = args.network
    if network == 'scratch':
        s = []
    #N = 50
    
    
    
    # provinces = ['Nova Scotia','New Brunswick'] # OVERWRITE
    # N = 50

    #provinces = ['New Brunswick']
    #N = 75

    #provinces = ['Nova Scotia']
    #N = 250

    #provinces = ['Newfoundland and Labrador']
    #N = 400
    
    dict_size = {'New Brunswick':150, 'Prince Edward Island':70, 'Nova Scotia':200, 'Newfoundland and Labrador':200,
            'Alberta':500, 'British Columbia':1000, 'Manitoba':400, 'Saskatchewan':400, 'Ontario':500, NA_gdf.iloc[5]["Name"]:500,
            'Yukon':200, 'Northwest Territories':100, 'Nunavut':100}

    for province in provinces:
        print(province)
        if args.add == 'default':
            N = dict_size[province]
        else:
            N = int(args.add)
        #N = 220 # OVERWRITE        print(f"Design network for up to {N} stations")
        print(f"Add up to {N} stations...")
        data_xr = xr.open_dataset(os.path.join(OUT, f"data-{province.replace(' ','_')}_mean.nc"))
        X_AM = data_xr.tp#.values
        data_xr = xr.open_dataset(os.path.join(OUT, f"data-{province.replace(' ','_')}_pop.nc"))
        X_POP = data_xr.tp#.values
        data_xr = xr.open_dataset(os.path.join(OUT, f"data-{province.replace(' ','_')}_error.nc"))
        data_xr = data_xr**2
        if args.measurement == 'variable':
            measunc = 'variable'
            sigma_A_n = np.sqrt(data_xr.tp.mean(dim='time').values)
        else:
            measunc = 'uniform'
            sigma_A_n = np.array([0.05]) # 5% error by default
        area_of_interest = mask_xr.geographic_mask.values == NA_gdf.loc[NA_gdf["Name"]==province].index[0]
        
        uncertainty_samples = design_network_with_error(s, 
                       area_of_interest, 
                       X_AM, X_POP,
                       N, NA_gdf, province, measunc, sigma_A_n=sigma_A_n)
        #print("done.")
        #uncertainty_df = pd.read_csv(os.path.join(TMP, f"NetworkDesign-{province.replace(' ','_')}.csv"), index_col=0)
        #fig = plt.figure()
        #plt.plot(uncertainty_samples)
        #plt.xlabel('Number of stations')
        #plt.ylabel('Total Uncertainty (maximum)')
        #plt.show()              
        #fig.savefig(os.path.join(FIG,'OND-Experiment1-20201020',f'From_{network}',f"Uncertainty-{province.replace(' ','_')}.png"))
        #plt.close()
