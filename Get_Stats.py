# -*- coding: utf-8 -*-
"""
Created on Fri Dec 10 11:57:56 2021

@author: BrunetD
"""

import xarray as xr
from matplotlib import pyplot as plt
import numpy as np
import os
from glob import glob

DATA_PATH = 'DATA'
PLOT = False
T = 0.04

# %% Compute precip amount parameters
def prior_stats(DS, T=0.04):
    E_X= DS.tp.mean(axis=0)
    V_X = DS.tp.var(axis=0)
    p = np.mean(DS.tp>T, axis=0)
    E_A = E_X/p
    V_A = V_X/p-(1-p)*E_A**2
    sigma2_A = np.log(V_A/E_A+1)
    sigma_A = np.sqrt(sigma2_A)
    mu_A = np.log(E_A) - sigma2_A/2
    DS_prior = xr.Dataset({'mu_A':mu_A, 
                           'sigma_A':sigma_A, 
                           'p':p, 
                           'E_TP':E_X, 
                           'V_TP':V_X})
    return DS_prior

# %% Read data
list_of_files = glob(os.path.join(DATA_PATH, 'data-*.nc'))
provinces = [os.path.basename(file_).split('.')[0].split('-')[1] for file_ in list_of_files]
provinces = np.unique(np.array(provinces))
print(provinces)

for province in provinces:
    print(province)
    try:
        DS = xr.open_dataset(os.path.join(DATA_PATH, f'data-{province}.nc'))

        # %% Call
        DS_prior = prior_stats(DS, T=T)
        DS_prior.to_netcdf(os.path.join(DATA_PATH, f'prior-stats-{province}.nc'))
    except FileNotFoundError:
        print("Could not run...")
# %% log-normal distribution (linear scale)
if PLOT:
    from scipy.stats import lognorm
    x = np.linspace(lognorm.ppf(0.01, s=DS_prior.sigma_A[0], loc=DS_prior.mu_A[0]),
                    lognorm.ppf(0.99, s=DS_prior.sigma_A[0], loc=DS_prior.mu_A[0]), 999)
    plt.figure()
    plt.plot(x, lognorm.pdf(x, s=DS_prior.sigma_A[0], loc=DS_prior.mu_A[0]),
          'r-', lw=5, alpha=0.6, label='norm pdf')
    plt.hist(DS.tp[:,0], bins=50, density=True)
    
# %% log-normal distribution (log-scale)
if PLOT:
    from scipy.stats import norm
    X_val = DS.tp
    X_log = np.where(X_val<=T, np.nan, np.log(X_val))
    x = np.linspace(norm.ppf(0.001, scale=DS_prior.sigma_A[0], loc=DS_prior.mu_A[0]),
                    norm.ppf(0.995, scale=DS_prior.sigma_A[0], loc=DS_prior.mu_A[0]), 999)
    plt.figure()
    plt.plot(x, norm.pdf(x, scale=DS_prior.sigma_A[0], loc=DS_prior.mu_A[0]),
          'r-', lw=5, alpha=0.6, label='norm pdf')
    plt.hist(X_log[:,0], bins=50, density=True)

# %% Check variance statistic
if PLOT:
    V_X = DS.tp.var(axis=0)
    p = DS_prior.p
    mu_A = DS_prior.mu_A
    sigma_A = DS_prior.sigma_A
    sigma2_A = sigma_A**2
    E_A_est = np.exp(mu_A+sigma2_A/2)
    V_A_est = (np.exp(sigma2_A)-1)*np.exp(mu_A+sigma2_A/2)
    V_X_est = p*V_A_est+p*(1-p)*E_A_est**2

    plt.figure()
    plt.plot(np.sqrt(V_X), np.sqrt(V_X_est), '.')
    plt.axis([0,20,0,20])
    plt.xlabel('Uncertainty (from samples)')
    plt.ylabel('Uncertainty (estimated)')
    plt.grid()
    ax = plt.gca()
    plt.xticks([0,5,10,15,20])
    plt.yticks([0,5,10,15,20])
    ax.set_aspect('equal', adjustable='box')
