# -*- coding: utf-8 -*-
"""
Created on Tue Nov 29 15:52:08 2022

@author: BrunetD
"""

import os
import urllib

DATA_PATH = os.path.join("DATA", "SOURCE")
countries_path = "https://www.naturalearthdata.com/http//www.naturalearthdata.com/download/10m/cultural/ne_10m_admin_0_countries.zip"
provinces_path = "https://www.naturalearthdata.com/http//www.naturalearthdata.com/download/10m/cultural/ne_10m_admin_1_states_provinces.zip"
graticules1_path = "https://www.naturalearthdata.com/http//www.naturalearthdata.com/download/10m/physical/ne_10m_graticules_1.zip"
graticules30_path = "https://www.naturalearthdata.com/http//www.naturalearthdata.com/download/10m/physical/ne_10m_graticules_30.zip"
for src_path in [countries_path, provinces_path, graticules1_path, graticules30_path]:
    name_ext = os.path.basename(src_path)
    name = name_ext.split('.')[0]
    dest_path = os.path.join(DATA_PATH, "Geographical", "NaturalEarth", name)
    print(dest_path)
    os.makedirs(dest_path, exist_ok=True)
    try:
        urllib.request.urlretrieve(src_path, os.path.join(dest_path, name_ext))
    except:
        print("Request failed!") # Note: data were downloaded manually as all the urllib request failed
