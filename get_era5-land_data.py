# -*- coding: utf-8 -*-
"""
This script downloads ERA5-Land data using the Copernicus API
A free account on the Copernicus Climate Data Store is required. 
Data can be browsed and manually downloaded here: 
https://cds.climate.copernicus.eu/cdsapp#!/dataset/reanalysis-era5-land?tab=overview
Instructions to setup the cdsapi key can be found here:
https://cds.climate.copernicus.eu/api-how-to
"""

import cdsapi 
import os
    
DATA_PATH = os.path.join('DATA', 'SOURCE', 'ERA5-Land')
os.makedirs(DATA_PATH, exist_ok=True)

name = {'tp':'total_precipitation',
        'u10':'10m_u_component_of_wind',
        'v10':'10m_v_component_of_wind',
        't2m':'2m_temperature'}
fields = name.keys()

c = cdsapi.Client()

# Specify path in which to save raw ERA5-Land data
years = [str(year) for year in range(2012,2022)]
months = [str(month).zfill(2) for month in range(1,13)]
days = [str(day).zfill(2) for day in range(1,32)]
times = [str(hour).zfill(2)+':00' for hour in range(0,24)]
for field in fields:
    for year in years:
        for month in months:
            foldername = os.path.join(DATA_PATH, field, 'hourly')
            os.makedirs(foldername, exist_ok=True)
            filename = os.path.join(foldername,
                         f'ERA5-Land_{field}_NA40N_hourly_{year}-{month}.nc')
            if not os.path.isfile(filename):
                c.retrieve(
                    'reanalysis-era5-land',
                    {   
                        'variable': [name[field]
                        ],
                        'year': [year],
                        'month': [month],
                        'day': days,
                        'time': times,
                        'area': [
                            85, -170, 40,
                            -50,
                        ],
                        'format': 'netcdf',
                    },
                    filename)
            else:
                print(f"{filename} is already downloaded")
