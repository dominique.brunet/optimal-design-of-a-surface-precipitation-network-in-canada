#!/usr/bin/env python
# coding: utf-8

# ### Import packages



import numpy as np

import os

import pandas as pd

import statsmodels.api as sm

import scipy.stats as ss


# %% Paths
DATA_PATH = os.path.join('DATA', 'PREPROCESS')
RESULTS_PATH = os.path.join('DATA', 'RESULTS', 'SPICE')
os.makedirs(RESULTS_PATH, exist_ok=True)

# %% Read data
data_daily_long = pd.read_csv(os.path.join(DATA_PATH, 'SPICE', 'daily_post-spice_data.csv'), index_col=0)

# %% Fitting functions:
def linear_fit(data, x_name='x', y_name='y'):
    X = data[[x_name]]
    Y = data[[y_name]]
    X = sm.add_constant(X)
    model = sm.OLS(Y,X)
    results = model.fit()
    params_df = results.conf_int()
    params_df = results.conf_int().rename(columns={0:'lb', 1:'ub'})
    params_df['params'] = results.params
    params_df = params_df.rename(index={'const':'b',x_name:'a'})
    return params_df

def linear_eval(x, params_dict):
    return params_dict['a']*x + params_dict['b']

def multiplicative_fit(data, x_name, y_name): # y ~ a*x
    X = data[[x_name]]
    Y = data[[y_name]]
    model = sm.OLS(Y,X)
    results = model.fit()
    params_df = results.conf_int()
    params_df = results.conf_int().rename(columns={0:'lb', 1:'ub'})
    params_df['params'] = results.params
    params_df = params_df.rename(index={x_name:'a'})
    return params_df

def multiplicative_eval(x, params_dict):
    return params_dict['a']*x

def powerlaw_fit(data, x_name='x', y_name='y'):
    # Remove any zero values before taking log
    data = data.loc[(data[x_name]>0)&(data[y_name]>0)]
    X = np.log(data[[x_name]])
    Y = np.log(data[[y_name]])
    X = sm.add_constant(X)
    model = sm.OLS(Y,X)
    results = model.fit()
    results.params['const'] = np.exp(results.params['const'])
    params_df = results.conf_int()
    params_df = results.conf_int().rename(columns={0:'lb', 1:'ub'})
    params_df['params'] = results.params
    params_df = params_df.rename(index={'const':'c',x_name:'d'})
    return params_df

def check_powerlaw(params):
    if (params.at['d', 'params']<0) or (params.at['d', 'params']>1) or (params.at['c', 'params']<0): # decreasing or convex fitting function 
        params.at['c', 'params'] = 0
        params.at['d', 'params'] = 1 # i.e. don't do any power law fit
        params.at['c', 'lb'] = np.nan
        params.at['c', 'ub'] = np.nan
        params.at['d', 'lb'] = np.nan
        params.at['d', 'ub'] = np.nan
    return params

def check_linear(params):
    if (params.at['b', 'params']<0): # decreasing or convex fitting function 
        params.at['b', 'params'] = 0
        params.at['b', 'lb'] = np.nan
        params.at['b', 'ub'] = np.nan
    if (params.at['a', 'params']<0): # decreasing or convex fitting function 
        params.at['a', 'params'] = 0
        params.at['a', 'lb'] = np.nan
        params.at['a', 'ub'] = np.nan
    return params

def powerlaw_eval(x, params_dict):
    return params_dict['c']*x**params_dict['d']

def eval_corrected(x, params):
    return x+powerlaw_eval(x, params[['c','d']])#+linear_eval(x, params[['a','b']])

# %% Fitting loop
wind_dict = {0:'low', 3:'medium', 6:'high'}
phase_dict = {-np.inf:'snow', -1:'mixed', 3:'rain'}

params_all = []
x = np.linspace(0,100,2001)
wind_bounds = [0,3,6,np.inf]
temp_bounds = [-np.inf, -1, 3, np.inf]
for (temp_lb, temp_ub) in zip(temp_bounds[:-1],temp_bounds[1:]):
    for (wind_lb, wind_ub) in zip(wind_bounds[:-1],wind_bounds[1:]):
        
        params_cat = pd.DataFrame(index=['wind speed','phase'], data={'lb':[np.nan,np.nan], 
                                                                      'ub':[np.nan,np.nan], 
                                                                      'params':[wind_dict[wind_lb], 
                                                                                phase_dict[temp_lb]]})
        data_selected = data_daily_long.loc[(data_daily_long['Air Temperature [degC]']<temp_ub)&
                                (data_daily_long['Air Temperature [degC]']>=temp_lb)&
                                (data_daily_long['Measured Precipitation [mm]']>0)&
                                (data_daily_long['Reference Precipitation [mm]']>0)&
                                (~data_daily_long['Outliers'])&
                                (data_daily_long['Wind Speed [m/s]']<wind_ub)&
                                (data_daily_long['Wind Speed [m/s]']>=wind_lb)]
        data_selected = data_selected.rename(columns={'Measured Precipitation [mm]':'measured',
                                  'Reference Precipitation [mm]':'reference'})

        N = len(data_selected)
        if N<30:
            print('Warning: small N')
        print(f"Wind: [{wind_lb}, {wind_ub}] m/s, Temp: [{temp_lb},{temp_ub}] degC (n = {N})")
        # Power law fit on reference - measured
        data_selected['residual'] = data_selected['reference'] - data_selected['measured']
        params_fit = powerlaw_fit(data_selected, 'measured', 'residual')
        params_fit = check_powerlaw(params_fit)
        print(params_fit)
        data_selected['corrected'] = eval_corrected(data_selected['measured'], params_fit['params'])
        # MSE params estimation (for large values)
        data_selected['error_residual'] = data_selected['reference'] - data_selected['corrected']
        data_selected['square_residual'] = data_selected['error_residual']**2
        perc_out = 0.99 # approximate percentage not-outliers
        perc_ci = 0.95
        data_selected_small_error = data_selected.sort_values(by='square_residual').iloc[0:int(perc_out*N)] # keep ~99% of data
        N_kept = len(data_selected_small_error)
        perc_kept = N_kept/N # exact percentage data kept
        perc_bound = perc_ci/perc_kept # needed percentage in c.i.
        ci_int = ss.norm.ppf((perc_bound+1)/2)
        params_error_large = linear_fit(data_selected_small_error, 'corrected', 'square_residual')
        params_error_large = check_linear(params_error_large)
        # RMSE bounds
        error_bound_large = ci_int*np.sqrt(linear_eval(x, params_error_large['params']))
        correction = eval_corrected(x, params_fit['params'])
        data_selected['log-error'] = np.log(data_selected['reference']) - np.log(data_selected['corrected'])
        data_selected_small = data_selected.loc[data_selected['corrected']<=1]
        gamma = np.std(data_selected_small['log-error'])
        params_error_small = pd.DataFrame(index=['gamma'], data={'lb':np.nan, 'ub':np.nan, 'params':gamma})
        params = params_cat.append(params_fit).append(params_error_small).append(params_error_large)
        params_all.append(params['params'])

# In[87]:
params_all = pd.DataFrame(params_all).rename(columns={'a':'alpha','b':'beta','c':'a','d':'b'})
params_all.reset_index().to_csv(os.path.join(RESULTS_PATH,'fitting_parameters.csv'))

# %% POP
precip_dict = {0:'none', 0.2:'trace', 3:'low', np.inf:'medium-heavy'}
k = 0 
params_all = []
wind_bounds = [0,3,6,np.inf]
temp_bounds = [-np.inf,-1,3,np.inf]
precip_bounds = [-np.inf,0,0.2,3,np.inf]
for (temp_lb, temp_ub) in zip(temp_bounds[:-1],temp_bounds[1:]):
    for (wind_lb, wind_ub) in zip(wind_bounds[:-1],wind_bounds[1:]):
        for (precip_lb, precip_ub) in zip(precip_bounds[:-1],precip_bounds[1:]):
            params_select = dict()
            # data selection
            data_selected = data_daily_long.loc[(data_daily_long['Air Temperature [degC]']<temp_ub)&
                                          (data_daily_long['Air Temperature [degC]']>=temp_lb)&
                                          (data_daily_long['Measured Precipitation [mm]']>precip_lb)&
                                          (data_daily_long['Measured Precipitation [mm]']<=precip_ub)&
                                          (~data_daily_long['Outliers'])&
                                          (data_daily_long['Wind Speed [m/s]']<wind_ub)&
                                          (data_daily_long['Wind Speed [m/s]']>=wind_lb)]
            n = len(data_selected.loc[data_selected['Reference Precipitation [mm]']==0])
            N = len(data_selected)
            params_select['wind speed'] = wind_dict[wind_lb] 
            params_select['phase'] = phase_dict[temp_lb]
            params_select['precip'] = precip_dict[precip_ub] 
            params_select['N'] = len(data_selected)
            if N<25:
                print(f"-- Small sample warning -- ")
            params_select['PoP'] = 1-n/N
            mu_A = np.log(data_selected.loc[data_selected['Reference Precipitation [mm]']>0, 
                                            'Reference Precipitation [mm]']).mean()
            params_select['mu_A'] = mu_A
            print(f"PoP = {params_select['PoP']} (E[log(P)] = {params_select['mu_A']}) for {params_select['wind speed']} wind speed, {params_select['phase']} precip phase, {params_select['precip']} precip amount (N = {N})")
            params_select = pd.DataFrame(index=[k], data=params_select)
            if len(params_all)==0:
                params_all = params_select
            else:
                params_all = params_all.append(params_select)
            k = k+1



# In[90]:
params_low = params_all.loc[params_all['precip']!='medium-heavy']
N = params_all.loc[params_all['precip']=='medium-heavy'].sum()['N']

p = params_all.loc[params_all['precip']=='medium-heavy'].mean()['PoP']

p_prime = (p*N+99)/(N+100)

params_heavy = pd.DataFrame(index=[26], data={'wind speed':'*', 'phase':'*', 'precip':'medium-heavy','N':N,'PoP':p_prime})

params_all = params_heavy.append(params_low)

# In[96]:
def compute_pop_unc(p, N):
    return np.sqrt(p*(1-p)/N)

params_all['PoP unc.'] = compute_pop_unc(params_all['PoP'],params_all['N'])
params_all['PoP lb'] = params_all['PoP'] - params_all['PoP unc.']
params_all['PoP ub'] = np.minimum(1, params_all['PoP'] + params_all['PoP unc.'])

params_all.to_csv(os.path.join(RESULTS_PATH,'pop_parameters.csv'))
