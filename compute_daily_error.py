# -*- coding: utf-8 -*-
"""
Created on Tue Oct 18 10:08:38 2022

@author: BrunetD
"""

import numpy as np
import os
import pandas as pd
from glob import glob
import scipy.stats as ss
import xarray as xr

DATA_PATH = os.path.join("DATA", "PREPROCESSED")
RESULTS_PATH = os.path.join("DATA", "RESULTS")
list_of_daily_files = glob(os.path.join(DATA_PATH, "ERA5-Land", '*', "daily", "ERA5-Land_*_daily_*.nc"))
list_of_daily_files = [os.path.basename(path) for path in list_of_daily_files]

# %% Ingest fitted parameters
fit_params = pd.read_csv(os.path.join(RESULTS_PATH, 'SPICE', 'fitting_parameters.csv'),
                         index_col=0)
pop_params = pd.read_csv(os.path.join(RESULTS_PATH, 'SPICE', 'pop_parameters.csv'),
                         index_col=0)

# %% Copy functions from spice_error.py
@np.vectorize
def get_phase(T):
  if T<-1:
    return 0#'snow'
  elif T>=3:
    return 2#'rain'
  else:
    return 1#'mixed'

@np.vectorize
def get_windcat(WS):
  if WS<3:
    return 0#'low'
  elif WS>=6:
    return 2#'high'
  else:
    return 1#'medium'

@np.vectorize
def get_precipcat(P):
  if P<=0.04: # threshold for ERA5-Land
    return 0#'none'
  elif P<=0.2:
    return 1#'trace'
  elif P<=3:
    return 2#'low'
  else:
    return 3#'high'

# %%
perc_ci = 0.95
perc_kept = 0.99 # percentage data kept
perc_bound = perc_ci/perc_kept # needed percentage in c.i.
ci_int = ss.norm.ppf((perc_bound+1)/2)

# %%
def linear_eval(x, a=1, b=0, **params):
    return a*x + b

def powerlaw_eval(x, c=1, d=1):
    return c*x**d

def eval_corrected(x, **params):
    return x+powerlaw_eval(x, **params)

def log_error_small(x, gamma=1, **params):
    return gamma

def log_error_large(x, **params):
    return ci_int/1.96*np.sqrt(linear_eval(x, **params))/x

def log_error(x, **params):
    return np.minimum(log_error_small(x, **params), log_error_large(x, **params))

# %%
def prepare_params_fit(params):
    X = np.empty((3,3,5))
    for (i, phase) in enumerate(['snow','mixed','rain']):
        for (j, windcat) in enumerate(['low','medium','high']):
            values = params.loc[(params['phase']==phase)&(params['wind speed']==windcat),
                             ['a','b','alpha','beta','gamma']].values[0]
            #print(f"{i},{j}: {values}")
            X[i,j,:] = values
    return X

def prepare_params_pop(params):
    X = np.empty((3,3,4,2))
    for (i, phase) in enumerate(['snow','mixed','rain']):
        for (j, windcat) in enumerate(['low','medium','high']):
            for (k, precip) in enumerate(['none','trace','low','high']):
                if precip!='high':
                    values = params.loc[(params['phase']==phase)&\
                                    (params['wind speed']==windcat)&\
                                    (params['precip']==precip),
                                 ['PoP','mu_A']].values[0]
                else: # POP = 100%, don't need mu_A
                    values = [1.0, np.nan]
                #print(f"{i},{j}: {values}")
                X[i,j,k,:] = values
    return X

def get_params_fit(params, phase_list, windcat_list):
    X = prepare_params_fit(params)
    a = X[phase_list, windcat_list, 0]
    b = X[phase_list, windcat_list, 1]
    alpha = X[phase_list, windcat_list,2]
    beta = X[phase_list, windcat_list, 3]
    gamma =  X[phase_list, windcat_list, 4]
    return [a, b, alpha, beta, gamma]

def get_params_pop(params, phase_list, windcat_list, precip_list):
    X = prepare_params_pop(params)
    pop = X[phase_list, windcat_list, precip_list, 0]
    mu_A = X[phase_list, windcat_list, precip_list, 1]
    return [pop, mu_A]

# %%
def correct_precip_and_error(params_fit, params_pop, T, WS, P): # single
    phase = get_phase(T)
    windcat = get_windcat(WS)
    precip = get_precipcat(P)
    POP, mu_A = get_params_pop(params_pop, phase, windcat, precip)
    [a, b, alpha, beta, gamma] = get_params_fit(params_fit, phase, windcat)
    P_corrected = eval_corrected(P, c=a, d=b)
    P_corrected[precip<2] = np.exp(mu_A[precip<2]) # for none or trace
    P_error = log_error(P_corrected, a=alpha, b=beta, gamma=gamma)
    return P_corrected, P_error, POP, phase, windcat

def vec2im(vec, shape, valid):
    im = np.zeros(shape)*np.nan
    imv = im.ravel()
    imv[valid] = vec
    im = imv.reshape(shape)
    return im

def np2xr(data_np, lat, lon, time):
    data_xr = xr.DataArray(data_np, 
                           coords={'latitude': lat,
                                   'longitude': lon,
                                   'time': time}, 
                           dims=["time", "latitude", "longitude"])
    return data_xr

def put_it_all_together(vec, shape, valid, tp, day):
    im = vec2im(vec, shape[1:], valid)
    im = im.reshape(1,shape[1],shape[2])
    return np2xr(im, tp.latitude, tp.longitude, tp.time[day-1:day])
    
# %% Loop through months
year_start = 2012
year_end = 2021
month_start = 1
month_end = 12
ERA5_folder = os.path.join(DATA_PATH, 'ERA5-Land')
tp_corr_folder = os.path.join(ERA5_folder, 'tp_corr', 'daily')
os.makedirs(tp_corr_folder, exist_ok=True)
for year in range(year_start, year_end+1):
    for month in range(month_start, month_end+1):
        month_str = str(month).zfill(2)
        tp_file = f'ERA5-Land_tp_NA40N_daily_{year}-{month_str}.nc'
        ws_file = f'ERA5-Land_ws_NA40N_daily_{year}-{month_str}.nc'
        t2m_file = f'ERA5-Land_t2m_NA40N_daily_{year}-{month_str}.nc'
        if tp_file not in list_of_daily_files:
            print(f"Skipping {tp_file}")
            continue
        print(tp_file)
        tp = xr.open_dataset(os.path.join(ERA5_folder, 'tp', 'daily', tp_file))
        ws = xr.open_dataset(os.path.join(ERA5_folder, 'ws', 'daily', ws_file))
        t2m = xr.open_dataset(os.path.join(ERA5_folder, 't2m', 'daily', t2m_file))
        shape = tp.tp.shape
        ndays = shape[0] # time
        phase_list, windcat_list, tp_mean_list, tp_error_list, tp_pop_list = [], [], [], [], []
        for day in range(1, ndays+1):
            print(day)
            tp_values = tp.tp.loc[tp.time.dt.day==day,:,:].values.ravel()
            ws_values = ws.ws.loc[ws.time.dt.day==day,:,:].values.ravel()
            t2m_values = t2m.t2m.loc[t2m.time.dt.day==day,:,:].values.ravel()
            valid = ~np.isnan(tp_values)
            tp_values = tp_values[valid]
            tp_values = np.maximum(0, tp_values)# enforce positive precip values
            ws_values = ws_values[valid]
            t2m_values = t2m_values[valid]
            P_corrected, P_error, POP, phase, windcat = correct_precip_and_error(
                                    params_fit=fit_params, 
                                    params_pop=pop_params, 
                                    WS=ws_values, 
                                    T=t2m_values, 
                                    P=tp_values)
            P_corrected_xr = put_it_all_together(P_corrected, shape, valid, tp, day)
            P_error_xr = put_it_all_together(P_error, shape, valid, tp, day)
            windcat_xr = put_it_all_together(windcat, shape, valid, tp, day)
            phase_xr = put_it_all_together(phase, shape, valid, tp, day)
            pop_xr = put_it_all_together(POP, shape, valid, tp, day)
            phase_list.append(phase_xr)
            windcat_list.append(windcat_xr)
            tp_mean_list.append(P_corrected_xr)
            tp_error_list.append(P_error_xr)
            tp_pop_list.append(pop_xr)
        P_corrected_xr = xr.concat(tp_mean_list, dim='time')
        P_error_xr = xr.concat(tp_error_list, dim='time')
        phase_xr = xr.concat(phase_list, dim='time')
        windcat_xr = xr.concat(windcat_list, dim='time')
        pop_xr = xr.concat(tp_pop_list, dim='time')
        tp_corr = P_corrected_xr.to_dataset(name='tp_mean')
        tp_corr['tp_error'] = P_error_xr
        tp_corr['phase'] = phase_xr
        tp_corr['windcat'] = windcat_xr
        tp_corr['pop'] = pop_xr
        tp_corr.to_netcdf(os.path.join(tp_corr_folder, tp_file))
            
