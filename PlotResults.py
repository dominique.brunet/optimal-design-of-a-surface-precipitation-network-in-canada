#!/usr/bin/env python
# coding: utf-8

# In[1]:


from datetime import datetime
import numpy as np
import os
import pandas as pd
import xarray as xr
from scipy.stats import norm
from PIL import ImageDraw, Image
from shapely.ops import transform

from mpl_toolkits.axes_grid1.inset_locator import inset_axes
import shapely.geometry as sg # Operations on shapes (points, lines and polygons)
from pyproj import CRS # Coordinate Reference System (map projections)
import geopandas as gpd # Geospatial extension of Pandas
from cartopy import crs as ccrs
    
import OND

from matplotlib import pyplot as plt
import matplotlib.cm as cm
from matplotlib.colors import LogNorm


# In[13]:


FIG = "FIG"


# In[61]:


DATA_PATH = "DATA"
TMP = "RESULTS"
OUT = "OUTPUTS"


# In[4]:


# This is the gridded part
lon_min = -170
lon_max = -50
lat_min = 40
lat_max = 85

# Resolution
delta = 0.1

# This is the months and years used for both the gridded and station data
month_start = 1
month_end = 12
#months = [1,2,3]
#month = 4
year_start = 2000
year_end = 2020


# In[5]:


# Size
ni = int(np.round((lat_max-lat_min)/delta)+1)
nj = int(np.round((lon_max-lon_min)/delta)+1)


# In[6]:


latitude = np.linspace(lat_max, lat_min, ni)
longitude = np.linspace(lon_min, lon_max, nj)


# In[7]:


# Map projection
ll_crs = CRS.from_proj4("+proj=longlat +ellps=WGS84 +datum=WGS84 +no_defs")
ll_crs


# In[8]:


#ae_crs = CRS.from_proj4('+proj=aea +lat_1=50 +lat_2=70 +lat_0=40 +lon_0=-99.5 +x_0=0 +y_0=0 +datum=NAD83 +units=m +no_defs')


# In[9]:


def vec2im(vec, nx, ny, mask):
    im = np.zeros((ny,nx))
    mask = mask.reshape(ny,nx)
    im[mask] = np.nan
    im[~mask] = vec.squeeze()
    return im


# In[17]:


mask_xr = xr.open_dataset(os.path.join(TMP, 'geographic_mask.nc'))


# In[18]:


NA_gdf = gpd.read_file(os.path.join(TMP,'Provinces-States.shp'))


def get_gdf(df, y='Latitude', x='Longitude', crs=ll_crs):
    df['geometry'] = [sg.Point(x,y) for (x,y) in df[[x, y]].values] # Iterate over coordinates, construct Shapely Point
    gdf = gpd.GeoDataFrame(df) # Make Pandas DataFrame a GeoPandas GeoDataFrame
    gdf.crs = crs # Initialize coordinate reference system (crs)
    return gdf

precip_station_table = pd.read_csv(os.path.join(DATA_PATH, "Stations", "SCDNA", "SCDNA_stations.csv"), index_col=0)
station_select = get_gdf(precip_station_table)

(i,j) = OND.get_station_index(station_select, lon_min, lat_max)

index = i*nj+j # linear index
index_u = np.unique(index)

i_max_all = list(index_u // nj)
j_max_all = list(index_u % nj)

s = list(index_u)

def plot_uncertainty(U, j_max, i_max, j_max_all, i_max_all, NA_gdf, name, latitude, longitude, vmin=0, vmax=10):
    fig = plt.figure(figsize=(15,15))
    ax = plt.gca()
    
    region_no = NA_gdf[NA_gdf['Name']==name].index[0]
    region = NA_gdf.iloc[region_no]

    (lon_min1, lat_min1, lon_max1, lat_max1) = region['geometry'].bounds
    ax.axis([lon_min1-1, lon_max1+1, lat_min1-1, lat_max1+1])

    
    #station_select.plot(ax=ax, marker='.', zorder=9, alpha=0.8, color='r')
    if (i_max is not None) & (j_max is not None):
        ax.plot(longitude[j_max], latitude[i_max], 'xr', zorder=10)
    if (len(i_max_all)>0) & (len(j_max_all)>0):
        ax.plot(longitude[j_max_all], latitude[i_max_all], '.r', zorder=10)
    #countries.plot(ax=ax, color='lightgrey', zorder=-1)
    #countries.loc[countries['ADMIN']=='Canada'].boundary.plot(ax=ax, color='dimgrey')
    NA_gdf.plot(ax=ax, color='lightgrey', linestyle='--', edgecolor='k')
    
    NA_gdf.loc[region_no-1:region_no].plot(ax=ax, color='none', linestyle='--', edgecolor='dimgrey', zorder=9)
    h = ax.imshow(U, 
                  vmin=vmin, vmax=vmax, 
                  extent=[lon_min-delta/2, lon_max+delta/2, lat_min-delta/2, lat_max+delta/2], 
                  zorder=2)
    cax = fig.add_axes([0.24, 0.15, 0.545, 0.025])
    fig.colorbar(h, cax=cax, orientation='horizontal')
    #plt.title(region['Name'])
    #provinces.loc[provinces['name']=='British Columbia'].boundary.plot(ax=ax, color='dimgrey', linestyle='--')
    #provinces.loc[provinces['name']=='Saskatchewan'].boundary.plot(ax=ax, color='dimgrey', linestyle='--')
    #ax.text(-115.5, 56.75, 'Alberta', color='dimgrey', fontsize=24)
    #ax.text(-117.75, 51.25, 'B.C.', fontsize=24, color='dimgrey')
    return fig

from RunONDprovinces_R1_season import compute_uncertainty as compute_uncertainty_season
from RunONDprovinces_R1_season import compute_prior as compute_prior_season
from RunONDprovinces_R1 import compute_uncertainty
from RunONDprovinces_R1 import compute_prior

def make_xarray(U_TP, y=None, x=None):
    da = xr.Dataset(
        data_vars=dict(uncertainty=(["y", "x"], U_TP)),
        coords=dict(
            Longitude=("x", x),
            Latitude=("y", y)
        ),
        attrs=dict(
            description="Uncertainty map",
            units="mm/day",
        ),
    )
    #da['crs'] = ll_crs
    return da

shapefile_path = os.path.join(DATA_PATH, 'Geographical', 'NaturalEarth', 'Cultural', 'Graticules', 
                             "ne_50m_graticules_1.shp")
graticules = gpd.read_file(shapefile_path)

def new_plot(da, name, NA_gdf, graticules, stations=None, vmin=0, vmax=5):
    fig = plt.figure(figsize=(15,15))
    
    region_no = NA_gdf[NA_gdf['Name']==name].index[0]
    region = NA_gdf.iloc[region_no]
    (lon_min, lat_min, lon_max, lat_max) = region['geometry'].bounds
    lon_0 = (lon_min+lon_max)/2
    lat_0 = (lat_min+lat_max)/2
    lat_1 = lat_min
    lat_2 = lat_max
    
    pyproj_crs = CRS.from_proj4(f'+proj=aea +lat_1={lat_1} +lat_2={lat_2} +lat_0={lat_0} +lon_0={lon_0} +x_0=0 +y_0=0 +datum=NAD83 +units=m +no_defs')
    cartopy_crs = ccrs.AlbersEqualArea(central_longitude=lon_0, central_latitude=lat_0, standard_parallels=(lat_1, lat_2), globe=None)
    
    NA_gdf_ae = NA_gdf.to_crs(pyproj_crs)
    region_ae = NA_gdf_ae.iloc[region_no]
    (x_min1, y_min1, x_max1, y_max1) = region_ae['geometry'].bounds
    
    ax = fig.add_subplot(111, projection=cartopy_crs)
    ax.axis([x_min1-100000, x_max1+100000, y_min1-100000, y_max1+100000])

    NA_gdf_ae.plot(ax=ax, color='lightgrey', linestyle='--', edgecolor='k')
    NA_gdf_ae.loc[NA_gdf['Name']==name].plot(ax=ax, color='none', linestyle='--', edgecolor='dimgrey', zorder=8)
    
    graticules_ae = graticules.to_crs(pyproj_crs)
    graticules_ae.plot(ax=ax, color='grey', linestyle='--',alpha=0.5, zorder=10)
    
    stations.to_crs(pyproj_crs).plot(ax=ax, color='r', marker='.', zorder=9)
    
    #tgt_x = xr.DataArray(np.arange(990, 1090), dims="x")
    #tgt_y = xr.DataArray(np.arange(330, 430), dims="y")
    #da.isel(x=tgt_x).isel(y=tgt_y)
    da.uncertainty.plot.pcolormesh(ax=ax, x='Longitude', y='Latitude', vmin=vmin, vmax=vmax,
            transform=ccrs.PlateCarree(),
            add_colorbar=False
           )
    ax.set_title('')
    return fig

def make_stations_gdf_station(df, idx_dict, latitude, longitude, ll_crs):  
    s = df.loc[np.isnan(df['Uncertainty'])]['Index'].values
    #idx_list = [idx_dict[sidx] for sidx in s]
    i_max_all = [int(sidx // nj) for sidx in s]
    j_max_all = [int(sidx % nj) for sidx in s]

    N = len(s)
    
    df['Longitude'] = [np.nan]*(len(df)-N) + list(longitude[j_max_all])
    df['Latitude'] = [np.nan]*(len(df)-N) + list(latitude[i_max_all])
    geometry_list = []
    for idx, row in df.iterrows():
        if not np.isnan(row['Longitude']):
            geometry_list.append(sg.Point((row['Longitude'],row['Latitude'])))
        else:
            geometry_list.append(np.nan)
    df['geometry'] = geometry_list
    gdf = gpd.GeoDataFrame(df) # Make Pandas DataFrame a GeoPandas GeoDataFrame
    gdf.crs = ll_crs # Initialize coordinate reference system (crs)
    return gdf, N

def make_stations_gdf_scratch(df, N, idx_dict, latitude, longitude, ll_crs):  
    s = df.iloc[1:N+1]['Index'].values
    #idx_list = [idx_dict[sidx] for sidx in s]
    i_max_all = [int(sidx // nj) for sidx in s]
    j_max_all = [int(sidx % nj) for sidx in s]

    N = len(s)
    
    df['Longitude'] = [np.nan]*(len(df)-N) + list(longitude[j_max_all])
    df['Latitude'] = [np.nan]*(len(df)-N) + list(latitude[i_max_all])
    geometry_list = []
    for idx, row in df.iterrows():
        if not np.isnan(row['Longitude']):
            geometry_list.append(sg.Point((row['Longitude'],row['Latitude'])))
        else:
            geometry_list.append(np.nan)
    df['geometry'] = geometry_list
    gdf = gpd.GeoDataFrame(df) # Make Pandas DataFrame a GeoPandas GeoDataFrame
    gdf.crs = ll_crs # Initialize coordinate reference system (crs)
    return gdf, N

def get_index_stuff(mask_xr, province):
    area_of_interest = mask_xr.geographic_mask.values == NA_gdf.loc[NA_gdf["Name"]==province].index[0]
    area_of_interest = area_of_interest.ravel()
    original_idx = np.arange(0, len(area_of_interest))
    original_idx = original_idx[area_of_interest]#[inclusion_zone]
    area_idx = np.arange(0, len(original_idx))
    idx_dict = {origin:new for (origin,new) in zip(original_idx, area_idx)}
    return original_idx, idx_dict


def get_index_list(province, network, idx_dict,  N=0, is_seasonal='season', is_variable='variable'):
    if is_seasonal=='season':
        file_name = os.path.join(TMP, 
                                 f"NetworkDesign-{network}-{province.replace(' ','_')}.csv")
    else:
        file_name = os.path.join(TMP, 
                         f"NetworkDesign-{network}-{province.replace(' ','_')}-{is_seasonal}-{is_variable}.csv")
    df = pd.read_csv(file_name, index_col=0)
    if network == 'SCDNA':
        s = df.loc[np.isnan(df['Uncertainty'])]['Index'].values
    elif network == 'scratch':
        s = df.iloc[1:N+1]['Index'].values
        s = list(s.astype(int))
    idx_list = [idx_dict[sidx] for sidx in s]
    return idx_list, df

def get_uncertainty(idx_list, province):
    data_xr = xr.open_dataset(os.path.join(OUT, f"data-{province.replace(' ','_')}_mean.nc"))
    X = data_xr.tp#.values
    data_xr = xr.open_dataset(os.path.join(OUT, f"data-{province.replace(' ','_')}_pop.nc"))
    X_POP = data_xr.tp#.values
    
    # ## Parameters
    sigma_POP_n = np.array([0.01])
    sigma_A_n = np.array([0.05])
    
    mu_A_x, sigma_A_x, mu_POP_x, sigma_POP_x = compute_prior(X, X_POP, sigma_A_n)
        
    X = np.log(X.values) # assert X > 0
    X_POP = X_POP.values

        
    uncertainty_TP_agg, L_POP, L_A = compute_uncertainty(idx_list, 
                                      X_POP, 
                                      mu_POP_x, 
                                      sigma_POP_x, 
                                      None, 
                                      X, 
                                      mu_A_x, 
                                      sigma_A_x, 
                                      None, 
                                      sigma_POP_n, 
                                      sigma_A_n)

    return uncertainty_TP_agg

def get_uncertainty_season(idx_list, province):
    data_xr = xr.open_dataset(os.path.join(OUT, f"data-{province.replace(' ','_')}_mean.nc"))
    X = data_xr.tp#.values
    data_xr = xr.open_dataset(os.path.join(OUT, f"data-{province.replace(' ','_')}_pop.nc"))
    X_POP = data_xr.tp#.values
    data_xr = xr.open_dataset(os.path.join(OUT, f"data-{province.replace(' ','_')}_error.nc"))
    data_xr = data_xr**2
    #sigma_A_n = np.sqrt(data_xr.tp.mean(dim='time').values)'
    data_column = data_xr.tp#[:,(data_xr.longitude==-65.6).values]
    
    # ## Parameters
    sigma_POP_n = 0.01
    
    sigma_A_n = {}
    sigma_A_n['cool'] = np.sqrt(data_column.loc[(data_column.time.dt.month<=4)|(data_column.time.dt.month>=11)].mean(dim='time').values)
    sigma_A_n['warm'] = np.sqrt(data_column.loc[(data_column.time.dt.month>4)&(data_column.time.dt.month<11)].mean(dim='time').values)

    X = {'warm': X.loc[(X.time.dt.month>=4)&(X.time.dt.month<=10)],
         'cool': X.loc[(X.time.dt.month<=3)|(X.time.dt.month>=11)]}
    X_POP = {'warm': X_POP.loc[(X_POP.time.dt.month>=4)&(X_POP.time.dt.month<=10)],
         'cool': X_POP.loc[(X_POP.time.dt.month<=3)|(X_POP.time.dt.month>=11)]}
    #X = {'warm': X.loc[(X.time.dt.month>4)&(X.time.dt.month<11)],
    #     'cool': X.loc[(X.time.dt.month<=4)|(X.time.dt.month>=11)]}
    #X_POP = {'warm': X_POP.loc[(X_POP.time.dt.month>4)&(X_POP.time.dt.month<11)],
    #     'cool': X_POP.loc[(X_POP.time.dt.month<=4)|(X_POP.time.dt.month>=11)]}
    LX = {'warm': len(X['warm'].time), 'cool': len(X['cool'].time)}
    
    mu_A_x, sigma_A_x, mu_POP_x, sigma_POP_x = {'warm':[], 'cool':[]}, \
                                                {'warm':[], 'cool':[]}, \
                                                {'warm':[], 'cool':[]}, \
                                                {'warm':[], 'cool':[]}
    for season in ['warm','cool']:
        mu_A_x[season], \
        sigma_A_x[season], \
        mu_POP_x[season], \
        sigma_POP_x[season] = compute_prior_season(X[season], 
                                            X_POP[season], 
                                            sigma_A_n[season])
        
        X[season] = np.log(X[season].values) # assert X > 0
        X_POP[season] = X_POP[season].values

        
    uncertainty_TP_season, L_POP, L_A, E_AM, V_AM = {'warm':[], 'cool':[]},\
    {'warm':[], 'cool':[]}, {'warm':[], 'cool':[]}, {'warm':[], 'cool':[]}, {'warm':[], 'cool':[]}
    for season in ['warm','cool']:
        uncertainty_TP_season[season], \
        L_POP[season], \
        L_A[season],\
        E_AM[season],\
        V_AM[season] = compute_uncertainty_season(idx_list, 
                                      X_POP[season], 
                                      mu_POP_x[season], 
                                      sigma_POP_x[season], 
                                      None, 
                                      X[season], 
                                      mu_A_x[season], 
                                      sigma_A_x[season], 
                                      None, 
                                      sigma_POP_n, 
                                      sigma_A_n[season])
    uncertainty_TP_agg = (uncertainty_TP_season['warm']*LX['warm'] + uncertainty_TP_season['cool']*LX['cool'])/(LX['warm']+LX['cool'])

    #uncertainty_TP_agg, L_POP, L_A = compute_uncertainty(idx_list, X_POP, mu_POP_x, sigma_POP_x, None, 
    #                                     X, mu_A_x, sigma_A_x, None, sigma_POP_n, sigma_A_n)
    return uncertainty_TP_agg

def reindex(uncertainty_TP_agg, ni, nj, original_idx):
    U_TP = np.zeros((ni*nj,))*np.nan
    U_TP[original_idx] = uncertainty_TP_agg
    U_TP = U_TP.reshape(ni,nj)
    return U_TP

network_type_dict = {'SCDNA':'', 'scratch':'', 
                     'no_uni':'-noseason-uniform', 
                     'no_var':'-noseason-variable'}

#province = "New Brunswick"
for province in NA_gdf["Name"].iloc[0:13]:
    province_ = province.replace(' ','_')
    original_idx, idx_dict = get_index_stuff(mask_xr, province)
    idx_ = dict()
    df_ = dict()
    idx_['SCDNA'], df_['SCDNA'] = get_index_list(province, 'SCDNA', idx_dict, is_seasonal='season', is_variable='variable')
    N = len(idx_['SCDNA'])
    idx_['no_uni'], df_['no_uni'] = get_index_list(province, 'scratch', idx_dict, N=N, is_seasonal='noseason', is_variable='uniform')
    idx_['no_var'], df_['no_var'] = get_index_list(province, 'scratch', idx_dict, N=N, is_seasonal='noseason', is_variable='variable')
    idx_['scratch'], df_['scratch'] = get_index_list(province, 'scratch', idx_dict, N=N, is_seasonal='season', is_variable='variable')
    for network_type in idx_.keys():
        uncertainty_TP_agg = get_uncertainty(idx_[network_type], province)
        U_TP = reindex(uncertainty_TP_agg, ni, nj, original_idx)
        xr_U_TP = make_xarray(U_TP, y=latitude, x=longitude)
        path_umap = os.path.join(OUT, f"Unc_map-{province_}-{network_type}_UNI.nc")
        xr_U_TP.to_netcdf(path_umap)
        xr_U_TP['crs'] = ll_crs
        station_gdf, N = make_stations_gdf_scratch(df_[network_type], N, idx_dict, latitude, longitude, ll_crs)
        fig = new_plot(xr_U_TP, province, NA_gdf, graticules, stations=station_gdf, vmin=0, vmax=10)
        fig.savefig(path_umap.replace(OUT,FIG).replace('.nc','.png'))
        print(f"{province} ({network_type}): max = {xr_U_TP.uncertainty.max().values}, mean = {xr_U_TP.uncertainty.mean().values}")
        network_type_ = network_type_dict[network_type]
        path_SCDNA = os.path.join(TMP,f"NetworkDesign-SCDNA-{province_}{network_type_}.csv")
        OND_table_SCDNA = pd.read_csv(path_SCDNA, index_col=0)
        index0 = OND_table_SCDNA.dropna().index[0]-2
        OND_table_SCDNA.iat[OND_table_SCDNA.dropna().index[0]-2,1] = np.sqrt(xr_U_TP.uncertainty.max().values)
        OND_table_SCDNA.to_csv(path_SCDNA.replace('.csv','2.csv'))
# %%

