#!/usr/bin/env python
# coding: utf-8

# %% Import packages
import numpy as np
import os
import pandas as pd

# %% PATHS 
DATA_PATH = os.path.join("DATA", "SOURCE", "SPICE")
os.makedirs(DATA_PATH, exist_ok=True))

# %% Download data from https://doi.org/10.1594/PANGAEA.907379

dataset_df = pd.DataFrame({907362:[31,'BrattsLake'],
             907363:[31,'BrattsLake'],
             907364:[43,'CARE'],
             907365:[37,'CARE'],
             907366:[19,'CaribouCreek'],
             907367:[27,'CaribouCreek'],
             907368:[21,'Formigal'],
             907369:[21,'Formigal'],
             907371:[43,'Haukeliseter'],
             907372:[43,'Haukeliseter'],
             907373:[31,'Marshall'],
             907374:[31,'Marshall'],
             907375:[25,'Sodylanka'],
             907376:[25,'Sodylanka'],
             907377:[26,'Weissflyhjoch'],
             907378:[26,'Weissflyhjoch'],
             },
             index=['skip_rows','site']).T

all_spice_data = []
for (key, row) in dataset_df.iterrows():
  filename = f'https://doi.pangaea.de/10.1594/PANGAEA.{key}?format=textfile'
  print(f"Downloading {filename}...")
  spice_data = pd.read_csv(filename,
              sep='\t', skiprows=row['skip_rows'], parse_dates=[0])
  print(spice_data.columns)
  spice_data['Site'] = row['site']
  all_spice_data.append(spice_data)
all_spice_data = pd.concat(all_spice_data)

# %% Keep only data with at least a pair of reference and station measurements
all_spice_data = all_spice_data.loc[(~np.isnan(all_spice_data['Precip [mm] (R2G)']))|
                   (~np.isnan(all_spice_data['Precip [mm] (R2P)']))]
all_spice_data = all_spice_data.loc[
                    (~np.isnan(all_spice_data['Precip [mm] (R3UG_adj_eq4_10m)']))|
                    (~np.isnan(all_spice_data['Precip [mm] (R3AG_adj_eq4_10m)']))|
                    (~np.isnan(all_spice_data['Precip [mm] (R3UP_adj_eq4_10m)']))|
                    (~np.isnan(all_spice_data['Precip [mm] (R3AP_adj_eq4_10m)']))
                    ]

# %% 
all_spice_data.to_csv(os.path.join(DATA_PATH, 'post-spice_data.csv'))
