#!/usr/bin/env python
# coding: utf-8

import os
import xarray as xr
import numpy as np
import pandas as pd
import geopandas as gpd # Geospatial extension of Pandas
from pyproj import CRS # Coordinate Reference System (map projections)
import shapely.geometry as sg # Operations on shapes (points, lines and polygons)
from datetime import datetime

DATA_PATH = os.path.join('DATA', 'SOURCE')
STATIONS_PATH = os.path.join(DATA_PATH, "Stations", "SCDNA")
STATIONS_PATH_OUT = os.path.join(DATA_PATH, "PREPROCESSED", "Stations")
os.makedirs(STATIONS_PATH_OUT, exist_ok=True)

# Bounding box
lat_min = 40
lat_max = 85
lon_min = -170
lon_max = -50
delta = 0.1

# ## Get SCDNA data
# Data downloaded on Zenodo..
scdna_file = os.path.join(STATIONS_PATH, "SCDNA_v1.1.nc4")
scdna_out = os.path.join(STATIONS_PATH_OUT, "SCDNA_stations.csv")

ll_crs = CRS.from_proj4("+proj=longlat +ellps=WGS84 +datum=WGS84 +no_defs")
def get_gdf(df, y='Latitude', x='Longitude', crs=ll_crs):
    df['geometry'] = [sg.Point(x,y) for (x,y) in df[[x, y]].values] # Iterate over coordinates, construct Shapely Point
    gdf = gpd.GeoDataFrame(df) # Make Pandas DataFrame a GeoPandas GeoDataFrame
    gdf.crs = crs # Initialize coordinate reference system (crs)
    return gdf

# xarray is used to open NetCDF file
print(f"Reading data from {scdna_file}")
SCDNA = xr.open_dataset(scdna_file)

# IDs are formatted as strings
IDs = SCDNA.ID.values.astype(str)
sources = [c0+c1 for (c0,c1) in zip(IDs[0,:],IDs[1,:])]
stationid = [c2+c3+c4+c5+c6+c7+c8+c9+c10+c11+c12 for (c2,c3,c4,c5,c6,c7,c8,c9,c10,c11,c12)\
        in zip(IDs[2,:],IDs[3,:],IDs[4,:],IDs[5,:],IDs[6,:],IDs[7,:],IDs[8,:],IDs[9,:],IDs[10,:],\
        IDs[11,:],IDs[12,:])]

# Only stations with precip data are kept
precip_stations = ~np.isnan(SCDNA.prcp.values[:,0])

# A meta-data table is created for station data
station_table = pd.DataFrame(data={'Source':sources,
                                   'ID':stationid,
                                   'Latitude':SCDNA.LLE.values[0,:],
                                   'Longitude':SCDNA.LLE.values[1,:],
                                   'Elevation':SCDNA.LLE.values[2,:]})
# OID is the Original ID used in SCDNA dataset
station_table['OID'] = station_table['Source'] + station_table['ID']
# Filter meta-data to only include stations with precip data
precip_station_table = station_table[precip_stations]

location_select_station = (precip_station_table['Latitude']>=lat_min-delta/2) & \
        (precip_station_table['Latitude']<lat_max+delta/2)  & \
        (precip_station_table['Longitude']>=lon_min-delta/2) & \
        (precip_station_table['Longitude']<lon_max+delta/2)

station_select = get_gdf(precip_station_table.loc[location_select_station])

# We could add country and province/state columns to help with data filtering.
print(f"Writing metadata to {scdna_out} and {scdna_out.replace('.csv','.shp')}")
station_select.to_csv(scdna_out)
station_select.to_file(scdna_out.replace(".csv",".shp"))

prcp = SCDNA.prcp.loc[precip_stations][location_select_station,:]

def get_date(date):
    date_str = str(int(date))
    year = int(date_str[0:4])
    month = int(date_str[4:6])
    day = int(date_str[6:8])
    date_dt = datetime(year, month, day)
    return date_dt

dates = [get_date(date) for date in SCDNA.date]


# %% Create dataset for precipitation
da = xr.Dataset(
    data_vars=dict(tp=(["station", "date"], prcp.values)),
    coords=dict(
        longitude=(["station"], station_select.Longitude.values),
        latitude=(["station"], station_select.Latitude.values),
        date=dates,
    ),
    attrs=dict(
        description="Total Precipitation",
        units="mm",
    ),
)

print("Saving data in SCDNA_data.nc")
da.to_netcdf(os.path.join(STATIONS_PATH_OUT, "SCDNA_data.nc"))
