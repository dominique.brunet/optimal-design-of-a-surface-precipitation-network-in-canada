#!/usr/bin/env python
# coding: utf-8

# ### Import packages
import numpy as np

import os

import pandas as pd

from datetime import datetime, timedelta

import statsmodels.api as sm

import scipy.stats as ss

# %%
DATA_PATH = os.path.join("DATA", "SOURCE", "SPICE")
DATA_PATH_OUT = os.path.join("DATA", "PREPROCESSED", "SPICE")
os.makedirs(DATA_PATH_OUT, exist_ok=True)

# %% Prepare data
all_spice_data = pd.read_csv(os.path.join(DATA_PATH, 'post-spice_data.csv'), 
                             index_col=0, parse_dates=[1])

all_spice_data['doy'] = all_spice_data['Date/Time'].dt.dayofyear
all_spice_data['year'] = all_spice_data['Date/Time'].dt.year

all_spice_data['Reference Precipitation [mm]'] = all_spice_data[['Precip [mm] (R2G)','Precip [mm] (R2P)']].fillna(0).sum(axis=1)

# %% Accumulate daily precipitation
data_daily = all_spice_data.groupby(by=['year','doy','Site']).agg({
                                                      'Reference Precipitation [mm]':lambda x: x.sum(skipna=False), 
                                                      'Precip [mm] (R3AG)':lambda x: x.sum(skipna=False),
                                                      'Precip [mm] (R3UG)':lambda x: x.sum(skipna=False),
                                                      'Precip [mm] (R3AP)':lambda x: x.sum(skipna=False),
                                                      'Precip [mm] (R3UP)':lambda x: x.sum(skipna=False),
                                                      'Precip [mm] (R3AG_adj_eq4_10m)':lambda x: x.sum(skipna=False),
                                                      'Precip [mm] (R3UG_adj_eq4_10m)':lambda x: x.sum(skipna=False),
                                                      'Precip [mm] (R3AP_adj_eq4_10m)':lambda x: x.sum(skipna=False),
                                                      'Precip [mm] (R3UP_adj_eq4_10m)':lambda x: x.sum(skipna=False),
                                                      'TTT [°C] (at 1.5m height)':lambda x: x.mean(skipna=False),
                                                      'ff [m/s] (at 10m height)':lambda x: x.mean(skipna=False)})

# %% Set date
data_daily = data_daily.reset_index()
data_daily['Date'] = [datetime(year-1,12,31)+timedelta(doy) for (doy, year) in zip(data_daily['doy'], data_daily['year'])]

# %% Error
data_daily['Precip Error (R3AG) [mm]'] = data_daily['Precip [mm] (R3AG)'] - data_daily['Reference Precipitation [mm]']
data_daily['Precip Error (R3UG) [mm]'] = data_daily['Precip [mm] (R3UG)'] - data_daily['Reference Precipitation [mm]']
data_daily['Precip Error (R3AP) [mm]'] = data_daily['Precip [mm] (R3AP)'] - data_daily['Reference Precipitation [mm]']
data_daily['Precip Error (R3UP) [mm]'] = data_daily['Precip [mm] (R3UP)'] - data_daily['Reference Precipitation [mm]']
data_daily['Precip Error (R3AG_adj_eq4_10m) [mm]'] = data_daily['Precip [mm] (R3AG_adj_eq4_10m)'] - data_daily['Reference Precipitation [mm]']
data_daily['Precip Error (R3UG_adj_eq4_10m) [mm]'] = data_daily['Precip [mm] (R3UG_adj_eq4_10m)'] - data_daily['Reference Precipitation [mm]']
data_daily['Precip Error (R3AP_adj_eq4_10m) [mm]'] = data_daily['Precip [mm] (R3AP_adj_eq4_10m)'] - data_daily['Reference Precipitation [mm]']
data_daily['Precip Error (R3UP_adj_eq4_10m) [mm]'] = data_daily['Precip [mm] (R3UP_adj_eq4_10m)'] - data_daily['Reference Precipitation [mm]']

# %%
data_daily_long = []
for (sensor) in ['G', 'P']:
    for shield in ['U','A']:
        data_daily_selected = data_daily.loc[data_daily['Precip Error (R3AG) [mm]'].notnull()&
               data_daily['Precip Error (R3UG) [mm]'].notnull(),
                                     ['Site',
                                      'Date',
                                      'TTT [°C] (at 1.5m height)',
                                      'Reference Precipitation [mm]',
                                      'ff [m/s] (at 10m height)',
                                      f'Precip [mm] (R3{shield}{sensor})',
                                      f'Precip Error (R3{shield}{sensor}) [mm]']].rename(columns={f'Precip [mm] (R3{shield}{sensor})':'Measured Precipitation [mm]', 
                                                                                                  f'Precip Error (R3{shield}{sensor}) [mm]':'Error [mm]',
                                                                                                  'TTT [°C] (at 1.5m height)':'Air Temperature [degC]',
                                                                                                  'ff [m/s] (at 10m height)':'Wind Speed [m/s]'})
        data_daily_selected['Sensor'] = sensor
        data_daily_selected['Shield'] = shield
        data_daily_long.append(data_daily_selected)
data_daily_long = pd.concat(data_daily_long)

# %% Check outliers
data_daily_long['Outliers'] = data_daily_long['Measured Precipitation [mm]']-data_daily_long['Reference Precipitation [mm]']>5
# Outliers are for data partially available on 2017-03-23 ar Haukeliseter site.

# %% Save
data_daily_long.loc[~data_daily_long['Outliers']].to_csv(os.path.join(DATA_PATH_OUT, 'daily_post-spice_data.csv'))
